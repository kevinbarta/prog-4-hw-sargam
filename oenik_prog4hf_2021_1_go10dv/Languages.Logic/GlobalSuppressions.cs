﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods")]
[assembly: SuppressMessage("Design", "CA1067:Override Object.Equals(object) when implementing IEquatable<T>")]
[assembly: SuppressMessage("Build", "CA1014:Mark assemblies with CLSCompliant")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores")]