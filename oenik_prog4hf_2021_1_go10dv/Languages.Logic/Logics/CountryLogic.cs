﻿namespace Languages.Logic.Logics
{
    using System.Collections.Generic;
    using System.Linq;
    using Languages.Data;
    using Languages.Repository;

    /// <summary>
    /// Logic for country.
    /// </summary>
    public class CountryLogic : ILogic<Country>
    {
        private IRepository<Country> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryLogic"/> class.
        /// </summary>
        public CountryLogic()
        {
            this.repository = new CountryRepository(LogicBase.DB);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryLogic"/> class.
        /// </summary>
        /// <param name="ir">Custom repository.</param>
        public CountryLogic(IRepository<Country> ir)
        {
            this.repository = ir;
        }

        /// <inheritdoc/>
        public IEnumerable<Country> GetAll()
        {
            return this.repository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Country GetOne(int id)
        {
            return this.repository.GetOne(id);
        }

        /// <inheritdoc/>
        public void Insert(Country entry)
        {
            this.repository.Insert(entry);
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.repository.Modify(id, value);
        }

        /// <inheritdoc/>
        public void Modify(int id, Country entry)
        {
            this.repository.Modify(id, entry);
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.repository.Remove(id);
        }
    }
}
