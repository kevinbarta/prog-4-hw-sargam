﻿namespace Languages.Logic.Logics
{
    using System.Collections.Generic;
    using System.Linq;
    using Languages.Data;
    using Languages.Repository;

    /// <summary>
    /// Logic for language.
    /// </summary>
    public class LangfamLangLinkLogic : ILogic<LangfamLangLink>
    {
        private readonly IRepository<LangfamLangLink> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LangfamLangLinkLogic"/> class.
        /// </summary>
        public LangfamLangLinkLogic()
        {
            this.repository = new LangfamLangLinkRepository(LogicBase.DB);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LangfamLangLinkLogic"/> class.
        /// </summary>
        /// <param name="ir">Custom repository.</param>
        public LangfamLangLinkLogic(IRepository<LangfamLangLink> ir)
        {
            this.repository = ir;
        }

        /// <inheritdoc/>
        public IEnumerable<LangfamLangLink> GetAll()
        {
            return this.repository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public LangfamLangLink GetOne(int id)
        {
            return this.repository.GetOne(id);
        }

        /// <inheritdoc/>
        public void Insert(LangfamLangLink entry)
        {
            this.repository.Insert(entry);
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.repository.Modify(id, value);
        }

        /// <inheritdoc/>
        public void Modify(int id, LangfamLangLink entry)
        {
            this.repository.Modify(id, entry);
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.repository.Remove(id);
        }
    }
}