﻿namespace Languages.Logic.Logics
{
    using System.Collections.Generic;
    using System.Linq;
    using Languages.Data;
    using Languages.Repository;

    /// <summary>
    /// Logic for language.
    /// </summary>
    public class CountryLangLinkLogic : ILogic<CountryLangLink>
    {
        private IRepository<CountryLangLink> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryLangLinkLogic"/> class.
        /// </summary>
        public CountryLangLinkLogic()
        {
            this.repository = new CountryLangLinkRepository(LogicBase.DB);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryLangLinkLogic"/> class.
        /// </summary>
        /// <param name="ir">Custom repository.</param>
        public CountryLangLinkLogic(IRepository<CountryLangLink> ir)
        {
            this.repository = ir;
        }

        /// <inheritdoc/>
        public IEnumerable<CountryLangLink> GetAll()
        {
            return this.repository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public CountryLangLink GetOne(int id)
        {
            return this.repository.GetOne(id);
        }

        /// <inheritdoc/>
        public void Insert(CountryLangLink entry)
        {
            this.repository.Insert(entry);
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.repository.Modify(id, value);
        }

        /// <inheritdoc/>
        public void Modify(int id, CountryLangLink entry)
        {
            CountryLangLink originalCountryLangLink = this.GetOne(id);
            typeof(ICountry).GetProperties().ToList().ForEach(property => property.SetValue(originalCountryLangLink, property.GetValue(entry)));
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.repository.Remove(id);
        }
    }
}