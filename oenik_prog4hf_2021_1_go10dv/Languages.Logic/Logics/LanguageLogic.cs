﻿namespace Languages.Logic.Logics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Languages.Data;
    using Languages.Repository;

    /// <summary>
    /// Logic for language.
    /// </summary>
    public class LanguageLogic : ILogic<Language>, ILanguageLogic
    {
        private readonly IRepository<Language> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageLogic"/> class.
        /// </summary>
        public LanguageLogic()
        {
            this.repository = new LanguageRepository(LogicBase.DB);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageLogic"/> class.
        /// </summary>
        /// <param name="ir">Custom repository.</param>
        public LanguageLogic(IRepository<Language> ir)
        {
            this.repository = ir;
        }

        /// <inheritdoc/>
        public IEnumerable<Language> GetAll()
        {
            return this.repository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public void Insert(Language entry)
        {
            this.repository.Insert(entry);
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.repository.Modify(id, value);
        }

        /// <inheritdoc/>
        public void Modify(int id, Language entry)
        {
            this.repository.Modify(id, entry);
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.repository.Remove(id);
        }

        /// <inheritdoc/>
        public IEnumerable<QLanguageFamilies> LanguageFamilies()
        {
            var q = from lang in new LanguageLogic().GetAll()
                    join lll in new LangfamLangLinkLogic().GetAll()
                    on lang.Id equals lll.LangId
                    join langfam in new LanguageFamilyLogic().GetAll()
                    on lll.LangfamId equals langfam.Id
                    select new QLanguageFamilies(lang.Name, langfam.Name);
            return q.ToList();
        }

        /// <inheritdoc/>
        public IEnumerable<QLanguageFamilies> LanguageFamiliesForTesting(IRepository<Language> il, IRepository<LangfamLangLink> illl, IRepository<LanguageFamily> ilf)
        {
            var q = from lang in il.GetAll()
                    join link in illl.GetAll()
                    on lang.Id equals link.LangId
                    join langfam in ilf.GetAll()
                    on link.LangfamId equals langfam.Id
                    select new QLanguageFamilies(lang.Name, langfam.Name);
            return q.ToList();
        }

        /// <inheritdoc/>
        public IEnumerable<QLanguagesByDifficulty> LanguagesByDifficulty()
        {
            var q = from lang in this.GetAll()
                    group lang.Name by lang.Difficulty
                    into g_lang
                    select new QLanguagesByDifficulty(g_lang.Key, g_lang.Count());
            return q;
        }

        /// <inheritdoc/>
        public IEnumerable<QOfficialLanguages> OfficialLanguages()
        {
            var q = from lang in this.GetAll()
                    join lll in new LangfamLangLinkLogic().GetAll()
                    on lang.Id equals lll.LangId
                    join langfam in new LanguageFamilyLogic().GetAll()
                    on lll.LangfamId equals langfam.Id
                    join cll in new CountryLangLinkLogic().GetAll()
                    on lang.Id equals cll.LangId
                    join country in new CountryLogic().GetAll()
                    on cll.CountryId equals country.Id
                    select new QOfficialLanguages(country.Name, lang.Name);
            return q;
        }

        /// <inheritdoc/>
        public IEnumerable<QNumberOfSpeakers> NumberOfSpeakers()
        {
            var q = from lang in this.GetAll()
                    group lang by lang.Difficulty
                    into g
                    select new QNumberOfSpeakers(g.Key, g.Select(x => Convert.ToInt64(x.NumberOfSpeakers)).Sum());
            return q;
        }

        /// <inheritdoc/>
        public Language GetOne(int id)
        {
            return this.repository.GetOne(id);
        }
    }
}