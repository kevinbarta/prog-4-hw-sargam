﻿namespace Languages.Logic.Logics
{
    using System.Collections.Generic;
    using System.Linq;
    using Languages.Data;
    using Languages.Repository;

    /// <summary>
    /// Logic for language.
    /// </summary>
    public class LanguageFamilyLogic : ILogic<LanguageFamily>
    {
        private readonly IRepository<LanguageFamily> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageFamilyLogic"/> class.
        /// </summary>
        public LanguageFamilyLogic()
        {
            this.repository = new LanguageFamilyRepository(LogicBase.DB);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageFamilyLogic"/> class.
        /// </summary>
        /// <param name="ir">Custom repository.</param>
        public LanguageFamilyLogic(IRepository<LanguageFamily> ir)
        {
            this.repository = ir;
        }

        /// <inheritdoc/>
        public IEnumerable<LanguageFamily> GetAll()
        {
            return this.repository.GetAll().ToList();
        }

        /// <inheritdoc/>
        public LanguageFamily GetOne(int id)
        {
            return this.repository.GetOne(id);
        }

        /// <inheritdoc/>
        public void Insert(LanguageFamily entry)
        {
            this.repository.Insert(entry);
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.repository.Modify(id, value);
        }

        /// <inheritdoc/>
        public void Modify(int id, LanguageFamily entry)
        {
            this.repository.Modify(id, entry);
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.repository.Remove(id);
        }
    }
}