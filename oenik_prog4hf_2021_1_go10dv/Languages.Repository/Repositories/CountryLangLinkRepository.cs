﻿namespace Languages.Repository
{
    using System.Linq;
    using Languages.Data;

    /// <summary>
    /// Repository for the countries.
    /// </summary>
    public class CountryLangLinkRepository : IRepository<CountryLangLink>
    {
        private DatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryLangLinkRepository"/> class.
        /// </summary>
        /// <param name="provider">Source of the DB context.</param>
        public CountryLangLinkRepository(DatabaseEntities provider)
        {
            this.db = provider;
        }

        /// <inheritdoc/>
        public IQueryable<CountryLangLink> GetAll()
        {
            return this.db.CountryLangLinks.AsQueryable();
        }

        /// <inheritdoc/>
        public CountryLangLink GetOne(int id)
        {
            return this.db.CountryLangLinks.Where(x => x.Id == id).First();
        }

        /// <inheritdoc/>
        public void Insert(CountryLangLink entry)
        {
            this.db.CountryLangLinks.Add(entry);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.db.CountryLangLinks.Where(x => x.Id == id).First().LangId = value;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, CountryLangLink entry)
        {
            CountryLangLink originalCountryLangLink = this.GetOne(id);
            typeof(Country).GetProperties().ToList().ForEach(property => property.SetValue(originalCountryLangLink, property.GetValue(entry)));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            CountryLangLink c = this.db.CountryLangLinks.Where(x => x.Id == id).First();
            this.db.CountryLangLinks.Remove(c);
            this.db.SaveChanges();
        }
    }
}
