﻿namespace Languages.Repository
{
    using System.Linq;
    using Languages.Data;

    /// <summary>
    /// Repository for the countries.
    /// </summary>
    public class LanguageFamilyRepository : IRepository<LanguageFamily>
    {
        private readonly DatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageFamilyRepository"/> class.
        /// </summary>
        /// <param name="provider">Source of the DB context.</param>
        public LanguageFamilyRepository(DatabaseEntities provider)
        {
            this.db = provider;
        }

        /// <inheritdoc/>
        public IQueryable<LanguageFamily> GetAll()
        {
            return this.db.LanguageFamilies.AsQueryable();
        }

        /// <inheritdoc/>
        public LanguageFamily GetOne(int id)
        {
            return this.db.LanguageFamilies.Where(x => x.Id == id).First();
        }

        /// <inheritdoc/>
        public void Insert(LanguageFamily entry)
        {
            this.db.LanguageFamilies.Add(entry);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.db.LanguageFamilies.Where(x => x.Id == id).First().NumberOfSpeakers = value;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, LanguageFamily entry)
        {
            LanguageFamily originalLanguageFamily = this.GetOne(id);
            typeof(Country).GetProperties().ToList().ForEach(property => property.SetValue(originalLanguageFamily, property.GetValue(entry)));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            LanguageFamily c = this.db.LanguageFamilies.Where(x => x.Id == id).First();
            this.db.LanguageFamilies.Remove(c);
            this.db.SaveChanges();
        }
    }
}
