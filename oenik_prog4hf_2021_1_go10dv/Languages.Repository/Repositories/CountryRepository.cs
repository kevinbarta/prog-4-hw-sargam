﻿namespace Languages.Repository
{
    using System.Linq;
    using Languages.Data;

    /// <summary>
    /// Repository for the countries.
    /// </summary>
    public class CountryRepository : IRepository<Country>
    {
        private DatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryRepository"/> class.
        /// </summary>
        /// <param name="provider">Source of the DB context.</param>
        public CountryRepository(DatabaseEntities provider)
        {
            this.db = provider;
        }

        /// <inheritdoc/>
        public IQueryable<Country> GetAll()
        {
            return this.db.Countries.AsQueryable();
        }

        /// <inheritdoc/>
        public Country GetOne(int id)
        {
            return this.db.Countries.Where(x => x.Id == id).First();
        }

        /// <inheritdoc/>
        public void Insert(Country entry)
        {
            this.db.Countries.Add(entry);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.db.Countries.Where(x => x.Id == id).First().Population = value;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, Country entry)
        {
            Country originalCountry = this.GetOne(id);
            typeof(Country).GetProperties().ToList().ForEach(property => property.SetValue(originalCountry, property.GetValue(entry)));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            Country c = this.db.Countries.Where(x => x.Id == id).First();
            this.db.Countries.Remove(c);
            this.db.SaveChanges();
        }
    }
}
