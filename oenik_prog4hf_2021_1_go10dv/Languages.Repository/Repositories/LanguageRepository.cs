﻿namespace Languages.Repository
{
    using System.Linq;
    using Languages.Data;

    /// <summary>
    /// Repository for the countries.
    /// </summary>
    public class LanguageRepository : IRepository<Language>
    {
        private readonly DatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageRepository"/> class.
        /// </summary>
        /// <param name="provider">Source of the DB context.</param>
        public LanguageRepository(DatabaseEntities provider)
        {
            this.db = provider;
        }

        /// <inheritdoc/>
        public IQueryable<Language> GetAll()
        {
            return this.db.Languages.AsQueryable();
        }

        /// <inheritdoc/>
        public Language GetOne(int id)
        {
            return this.db.Languages.Where(x => x.Id == id).First();
        }

        /// <inheritdoc/>
        public void Insert(Language entry)
        {
            this.db.Languages.Add(entry);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.db.Languages.Where(x => x.Id == id).First().NumberOfSpeakers = value;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, Language entry)
        {
            Language originalLanguage = this.GetOne(id);
            typeof(Country).GetProperties().ToList().ForEach(property => property.SetValue(originalLanguage, property.GetValue(entry)));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            Language c = this.db.Languages.Where(x => x.Id == id).First();
            this.db.Languages.Remove(c);
            this.db.SaveChanges();
        }
    }
}
