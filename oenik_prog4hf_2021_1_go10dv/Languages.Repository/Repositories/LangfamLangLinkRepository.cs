﻿namespace Languages.Repository
{
    using System.Linq;
    using Languages.Data;

    /// <summary>
    /// Repository for the countries.
    /// </summary>
    public class LangfamLangLinkRepository : IRepository<LangfamLangLink>
    {
        private DatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="LangfamLangLinkRepository"/> class.
        /// </summary>
        /// <param name="provider">Source of the DB context.</param>
        public LangfamLangLinkRepository(DatabaseEntities provider)
        {
            this.db = provider;
        }

        /// <inheritdoc/>
        public IQueryable<LangfamLangLink> GetAll()
        {
            return this.db.LangfamLangLinks.AsQueryable();
        }

        /// <inheritdoc/>
        public LangfamLangLink GetOne(int id)
        {
            return this.db.LangfamLangLinks.Where(x => x.Id == id).First();
        }

        /// <inheritdoc/>
        public void Insert(LangfamLangLink entry)
        {
            this.db.LangfamLangLinks.Add(entry);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, int value)
        {
            this.db.LangfamLangLinks.Where(x => x.Id == id).First().LangId = value;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Modify(int id, LangfamLangLink entry)
        {
            LangfamLangLink originalLangfamLangLink = this.GetOne(id);
            typeof(Country).GetProperties().ToList().ForEach(property => property.SetValue(originalLangfamLangLink, property.GetValue(entry)));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            LangfamLangLink c = this.db.LangfamLangLinks.Where(x => x.Id == id).First();
            this.db.LangfamLangLinks.Remove(c);
            this.db.SaveChanges();
        }
    }
}
