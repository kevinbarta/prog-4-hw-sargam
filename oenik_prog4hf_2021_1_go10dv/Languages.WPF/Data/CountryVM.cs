﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Languages.Data;

namespace Languages.WPF.Data
{
    /// <summary>
    /// Country view model.
    /// </summary>
    public class CountryVM : ObservableObject, ICountry
    {
        private int id;
        private string name;
        private int population;
        private string capital;
        private string continent;
        private int area;

        /// <inheritdoc/>
        public int Id { get => id; set => Set(ref id, value); }
        /// <inheritdoc/>
        public string Name { get => name; set => Set(ref name, value); }
        /// <inheritdoc/>
        public int Population { get => population; set => Set(ref population, value); }
        /// <inheritdoc/>
        public string Capital { get => capital; set => Set(ref capital, value); }
        /// <inheritdoc/>
        public string Continent { get => continent; set => Set(ref continent, value); }
        /// <inheritdoc/>
        public int Area { get => area; set => Set(ref area, value); }

        /// <summary>
        /// Copy from another country.
        /// </summary>
        /// <param name="country">The country to copy from.</param>
        /// <returns>The copied instance.</returns>
        public CountryVM CopyFrom(ICountry country)
        {
            typeof(ICountry).GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(country)));
            return this;
        }
    }
}
