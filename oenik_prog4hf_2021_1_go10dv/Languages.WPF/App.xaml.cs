﻿namespace Languages.WPF
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using Languages.WPF.BL;
    using Languages.WPF.UI;
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// App constructor.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIOC.Instance);

            MyIOC.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIOC.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIOC.Instance.Register<ICountryLogicVM, CountryLogicVM>();
        }

        private class MyIOC : SimpleIoc, IServiceLocator
        {
            public static MyIOC Instance { get; private set; } = new MyIOC();
        }

    }
}
