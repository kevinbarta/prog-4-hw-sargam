﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Languages.WPF.Data;

namespace Languages.WPF.BL
{
    /// <summary>
    /// Editor service.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edit a country.
        /// </summary>
        /// <param name="countryVM">Country to be edited.</param>
        /// <returns>Succeeded or not.</returns>
        bool EditCountry(CountryVM countryVM);
    }
}
