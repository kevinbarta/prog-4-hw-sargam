﻿using GalaSoft.MvvmLight.Messaging;
using Languages.Data;
using Languages.Logic;
using Languages.Logic.Logics;
using Languages.WPF.Data;
using Languages.WPF.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Languages.WPF.BL
{
    /// <summary>
    /// Country logic view model.
    /// </summary>
    public class CountryLogicVM : ICountryLogicVM
    {
        private const string LOGIC_RESULT = "LogicResult";

        private IEditorService editorService;
        private IMessenger messengerService;
        private CountryLogic countryLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryLogicVM"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="editorService">Editor service.</param>
        /// <param name="messengerService">Messenger service.</param>
        public CountryLogicVM(IEditorService editorService, IMessenger messengerService)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            countryLogic = new CountryLogic();
        }

        /// <summary>
        /// Adds a list of countries.
        /// </summary>
        /// <param name="countries">Countries.</param>
        public void AddCountry(IList<CountryVM> countries)
        {
            CountryVM countryVM = new CountryVM();
            if (editorService.EditCountry(countryVM))
            {
                countries.Add(countryVM);
                try
                {
                    countryLogic.Insert(new Country().CopyFrom(countryVM));
                    messengerService.Send("ADD OK", LOGIC_RESULT);
                }
                catch (DbUpdateException e)
                {
                    messengerService.Send(e.Message, LOGIC_RESULT);
                }
            }
            else
            {
                messengerService.Send("ADD CANCELED", LOGIC_RESULT);
            }

        }

        /// <summary>
        /// Delete a country.
        /// </summary>
        /// <param name="countries">The list to delete  from.</param>
        /// <param name="country">The country to delete.</param>
        public void DeleteCountry(IList<CountryVM> countries, CountryVM country)
        {
            if (country == null)
            {
                messengerService.Send("DELETE FAILED: Country is null.", LOGIC_RESULT);
            }
            else
            {
                try
                {
                    if (countries.Remove(country))
                    {
                        countryLogic.Remove(country.Id);
                        messengerService.Send("DELETE OK", LOGIC_RESULT);
                    }
                    else
                    {
                        throw new Exception("DELETE FAILED: Country was not found.");
                    }
                }
                catch (DbUpdateException e)
                {
                    messengerService.Send("DELETE: " + e.Message, LOGIC_RESULT);
                }
            }
        }

        /// <summary>
        /// Modify a country.
        /// </summary>
        /// <param name="country">Country.</param>
        public void ModifyCountry(CountryVM country)
        {
            if (country == null)
            {
                messengerService.Send("EDIT FAILED: Country was null.", LOGIC_RESULT);
                return;
            }

            CountryVM editedCountryVM = new CountryVM().CopyFrom(country);

            if (!AllContinents.Continents.Contains(editedCountryVM.Continent))
            {
                editedCountryVM.Continent = "Other";
            }

            if (editorService.EditCountry(editedCountryVM))
            {
                countryLogic.Modify(country.Id, new Country().CopyFrom(editedCountryVM));
                messengerService.Send("EDIT OK", LOGIC_RESULT);
                country.CopyFrom(editedCountryVM);
            }
            else
            {
                messengerService.Send("EDIT FAILED", LOGIC_RESULT);
            }
        }
    }
}
