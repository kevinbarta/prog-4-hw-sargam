﻿using Languages.Data;
using Languages.WPF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Languages.WPF.BL
{
    /// <summary>
    /// Country logic for the view model.
    /// </summary>
    public interface ICountryLogicVM
    {
        /// <summary>
        /// Adds a list of countries.
        /// </summary>
        /// <param name="countries">List of countries.</param>
        void AddCountry(IList<CountryVM> countries);

        /// <summary>
        /// Modifies a country.
        /// </summary>
        /// <param name="country">Country.</param>
        void ModifyCountry(CountryVM country);

        /// <summary>
        /// Deletes a country from a list.
        /// </summary>
        /// <param name="countries">List of countries.</param>
        /// <param name="country">The country to be deleted.</param>
        void DeleteCountry(IList<CountryVM> countries, CountryVM country);
    }
}
