﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Languages.WPF.Data
{
    /// <summary>
    /// Available continents.
    /// </summary>
    public static class AllContinents
    {
        /// <summary>
        /// Gets all the continents
        /// </summary>
        public static string[] Continents => new string[] { "Asia", "Africa", "Australia and Oceania", "North America", "South America", "Europe", "Other" };
    }
}
