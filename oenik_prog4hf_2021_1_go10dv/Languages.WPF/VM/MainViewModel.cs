﻿using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Languages.Data;
using Languages.Logic.Logics;
using Languages.WPF.BL;
using Languages.WPF.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Languages.WPF.VM
{
    /// <summary>
    /// Main view model.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private ICountryLogicVM logic;

        private CountryVM selectedCountryVM;

        /// <summary>
        /// Gets or sets selected country in the view model.
        /// </summary>
        public CountryVM SelectedCountryVM { get => selectedCountryVM; set => Set(ref selectedCountryVM, value); }

        /// <summary>
        /// Gets countries in the view model.
        /// </summary>
        public ObservableCollection<CountryVM> Countries { get; private set; }
        /// <summary>
        /// Gets addition command.
        /// </summary>
        public ICommand AddCmd { get; private set; }
        /// <summary>
        /// Gets deletion command.
        /// </summary>
        public ICommand DelCmd { get; private set; }
        /// <summary>
        /// Gets modification command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// Main view model.
        /// </summary>
        /// <param name="logic">Logic.</param>
        public MainViewModel(ICountryLogicVM logic)
        {
            this.logic = logic;
            CountryLogic countryLogic = new CountryLogic();
            IEnumerable<CountryVM> countries = countryLogic.GetAll().Select(x => (CountryVM)new CountryVM().CopyFrom(x));
            Countries = new ObservableCollection<CountryVM>(countries);

            if (IsInDesignMode)
            {
                // We could add if we wanted.
            }

            AddCmd = new RelayCommand(() => this.logic.AddCountry(Countries));
            ModCmd = new RelayCommand(() => this.logic.ModifyCountry(SelectedCountryVM));
            DelCmd = new RelayCommand(() => this.logic.DeleteCountry(Countries, SelectedCountryVM));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel() : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ICountryLogicVM>())
        {

        }
    }
}
