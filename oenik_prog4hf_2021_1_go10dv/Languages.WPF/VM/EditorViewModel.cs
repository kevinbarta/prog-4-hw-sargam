﻿using GalaSoft.MvvmLight;
using Languages.WPF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Languages.WPF.VM
{
    /// <summary>
    /// Editor view model.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private CountryVM countryVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            CountryVM = new CountryVM();

            if (IsInDesignMode)
            {
                CountryVM.Area = 0;
                CountryVM.Capital = "Capital";
                CountryVM.Continent = "Continent";
                CountryVM.Population = 0;
                CountryVM.Name = "Name";
            }
        }

        /// <summary>
        /// Gets continents.
        /// </summary>
        public Array Continents
        {
            get
            {
                return AllContinents.Continents;
            }
        }

        /// <summary>
        /// Gets or sets country view model.
        /// </summary>
        public CountryVM CountryVM { get => countryVM; set => Set(ref countryVM, value); }
    }
}
