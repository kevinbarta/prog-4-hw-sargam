﻿using Languages.WPF.BL;
using Languages.WPF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Languages.WPF.UI
{
    /// <summary>
    /// Editor service via Window.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Edit a country.
        /// </summary>
        /// <param name="countryVM">Country view model.</param>
        /// <returns>Succeeded or not.</returns>
        public bool EditCountry(CountryVM countryVM)
        {
            EditorWindow win = new EditorWindow(countryVM);
            return win.ShowDialog() ?? false;
        }
    }
}
