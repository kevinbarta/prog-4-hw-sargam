﻿namespace Languages.Data
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// DatabaseEntities.
    /// </summary>
    public partial class DatabaseEntities : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseEntities"/> class.
        /// </summary>
        public DatabaseEntities()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseEntities"/> class.
        /// </summary>
        /// <param name="options">Database context options.</param>
        public DatabaseEntities(DbContextOptions<DatabaseEntities> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets Countries.
        /// </summary>
        public virtual DbSet<Country> Countries { get; set; }

        /// <summary>
        /// Gets or sets CountryLangLinks.
        /// </summary>
        public virtual DbSet<CountryLangLink> CountryLangLinks { get; set; }

        /// <summary>
        /// Gets or sets LangfamLangLinks.
        /// </summary>
        public virtual DbSet<LangfamLangLink> LangfamLangLinks { get; set; }

        /// <summary>
        /// Gets or sets Languages.
        /// </summary>
        public virtual DbSet<Language> Languages { get; set; }

        /// <summary>
        /// Gets or sets LanguageFamilies.
        /// </summary>
        public virtual DbSet<LanguageFamily> LanguageFamilies { get; set; }

        /// <summary>
        /// Configuring the server.
        /// </summary>
        /// <param name="optionsBuilder">Checks if the server is configured already or not.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // Somehow should be made into relative path
                optionsBuilder.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=\"D:\\Documents\\GIT\\oenik_prog4hf_2021_1_go10dv\\oenik_prog4hf_2021_1_go10dv\\Languages.Data\\Database.mdf\"; Integrated Security=True");
            }
        }

        /// <summary>
        /// Creating the data model.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.Capital).IsUnicode(false);

                entity.Property(e => e.Continent).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            /*
            modelBuilder.Entity<CountryLangLink>(entity =>
            {
                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CountryLangLinks)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__country_l__count__2D27B809");

                entity.HasOne(d => d.Lang)
                    .WithMany(p => p.CountryLangLinks)
                    .HasForeignKey(d => d.LangId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__country_l__lang___2E1BDC42");
            });

            modelBuilder.Entity<LangfamLangLink>(entity =>
            {
                entity.HasOne(d => d.Lang)
                    .WithMany(p => p.LangfamLangLinks)
                    .HasForeignKey(d => d.LangId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__langfam_l__lang___2A4B4B5E");

                entity.HasOne(d => d.Langfam)
                    .WithMany(p => p.LangfamLangLinks)
                    .HasForeignKey(d => d.LangfamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__langfam_l__langf__29572725");
            });
            */

            modelBuilder.Entity<Language>(entity =>
            {
                entity.Property(e => e.Agglutinative)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Difficulty).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<LanguageFamily>(entity =>
            {
                entity.Property(e => e.IsoCode).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
