﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification")]
[assembly: SuppressMessage("Performance", "CA1813:Avoid unsealed attributes")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only")]
[assembly: SuppressMessage("Globalization", "CA1304:Specify CultureInfo")]
[assembly: SuppressMessage("Build", "CA1014:Mark assemblies with CLSCompliant")]