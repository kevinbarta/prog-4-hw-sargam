﻿namespace Languages.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Language family.
    /// </summary>
    [Table("language_family")]
    public partial class LanguageFamily
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageFamily"/> class.
        /// </summary>
        public LanguageFamily()
        {
            this.LangfamLangLinks = new HashSet<LangfamLangLink>();
        }

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        [Required]
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets IsoCode.
        /// </summary>
        [Required]
        [Column("iso_code")]
        [StringLength(10)]
        public string IsoCode { get; set; }

        /// <summary>
        /// Gets or sets NumberOfSpeakers.
        /// </summary>
        [Column("number_of_speakers")]
        public long NumberOfSpeakers { get; set; }

        /// <summary>
        /// Gets or sets RankByNoSpeakers.
        /// </summary>
        [Column("rank_by_no_speakers")]
        public int RankByNoSpeakers { get; set; }

        /// <summary>
        /// Gets or sets NumberOfLanguages.
        /// </summary>
        [Column("number_of_languages")]
        public int NumberOfLanguages { get; set; }

        /// <summary>
        /// Gets or sets RankByNoLanguages.
        /// </summary>
        [Column("rank_by_no_languages")]
        public int RankByNoLanguages { get; set; }

        /// <summary>
        /// Gets or sets LangfamLangLinks.
        /// </summary>
        [InverseProperty(nameof(LangfamLangLink.Langfam))]
        public virtual ICollection<LangfamLangLink> LangfamLangLinks { get; set; }
    }
}
