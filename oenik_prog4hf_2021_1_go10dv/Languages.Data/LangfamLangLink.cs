﻿#nullable disable

namespace Languages.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Language family and language link.
    /// </summary>
    [Table("langfam_lang_link")]
    public partial class LangfamLangLink
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets LangfamId.
        /// </summary>
        [Column("langfam_id")]
        public int LangfamId { get; set; }

        /// <summary>
        /// Gets or sets LangId.
        /// </summary>
        [Column("lang_id")]
        public int LangId { get; set; }

        /// <summary>
        /// Gets or sets Lang.
        /// </summary>
        [ForeignKey(nameof(LangId))]
        [InverseProperty(nameof(Language.LangfamLangLinks))]
        public virtual Language Lang { get; set; }

        /// <summary>
        /// Gets or sets Langfam.
        /// </summary>
        [ForeignKey(nameof(LangfamId))]
        [InverseProperty(nameof(LanguageFamily.LangfamLangLinks))]
        public virtual LanguageFamily Langfam { get; set; }
    }
}
