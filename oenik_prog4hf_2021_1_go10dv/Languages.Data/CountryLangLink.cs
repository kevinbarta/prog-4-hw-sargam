﻿namespace Languages.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// CountryLangLink class.
    /// </summary>
    [Table("country_lang_link")]
    public partial class CountryLangLink
    {
        /// <summary>
        /// Gets or sets CountryLangLink's id.
        /// </summary>
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets CountryLangLink's country id.
        /// </summary>
        [Column("country_id")]
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets CountryLangLink's lang_id.
        /// </summary>
        [Column("lang_id")]
        public int LangId { get; set; }

        /// <summary>
        /// Gets or sets  country id = country link.
        /// </summary>
        // [ForeignKey(nameof(CountryId))]
        [InverseProperty("CountryLangLinks")]
        public virtual Country Country { get; set; }

        /// <summary>
        /// Gets or sets lang id = language link.
        /// </summary>
        [ForeignKey(nameof(LangId))]
        [InverseProperty(nameof(Language.CountryLangLinks))]
        public virtual Language Lang { get; set; }
    }
}
