﻿namespace Languages.Data
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// True or False annotation.
    /// </summary>
    public class IsTorF : ValidationAttribute
    {
        /// <summary>
        /// Formatting the error message.
        /// </summary>
        /// <param name="name">The name of the attribute.</param>
        /// <returns>Text of the error.</returns>
        public override string FormatErrorMessage(string name)
        {
            return $"{name}'s value wasn't T or F.";
        }

        /// <summary>
        /// Returns whether the value is valid for true or false.
        /// </summary>
        /// <param name="value">Value passed.</param>
        /// <returns>Bool value.</returns>
        public override bool IsValid(object value)
        {
            return value.ToString().ToUpper() == "T" || value.ToString().ToUpper() == "F";
        }
    }
}
