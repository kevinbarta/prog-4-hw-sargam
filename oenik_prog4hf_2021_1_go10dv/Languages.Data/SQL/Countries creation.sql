﻿if object_id('country_lang_link', 'U') is not null drop table country_lang_link;
if object_id('langfam_lang_link', 'U') is not null drop table langfam_lang_link;
if object_id('language_family', 'U') is not null drop table language_family;
if object_id('country', 'U') is not null drop table country;
if object_id('language', 'U') is not null drop table language;

create table country (
	id int identity primary key not null,
	name varchar(40) not null,
	population int not null,
	capital varchar(40) not null,
	continent varchar(40) not null,
	area int not null
);

insert into country(name, population, capital, continent, area) values('Russia', 144500000, 'Moscow', 'Asia', 17100000)
insert into country(name, population, capital, continent, area) values('Jordan', 9702000, 'Amman', 'Asia', 89342)
insert into country(name, population, capital, continent, area) values('France', 66990000, 'Paris', 'Europe', 643801)
insert into country(name, population, capital, continent, area) values('Mexico', 129200000, 'Mexico City', 'America', 1973000)
insert into country(name, population, capital, continent, area) values('India', 1339000000, 'New Delhi', 'Asia', 3287000)
insert into country(name, population, capital, continent, area) values('Australia', 9773000, 'Canberra', 'Australia and Oceania', 7692000)
insert into country(name, population, capital, continent, area) values('UK', 66440000, 'London', 'Europe', 242495)
insert into country(name, population, capital, continent, area) values('China', 1386000000, 'Beijing', 'Asia', 9597000)
insert into country(name, population, capital, continent, area) values('USA', 327200000, 'Washington, D.C.', 'America', 9834000)
insert into country(name, population, capital, continent, area) values('Indonesia', 264000000, 'Jakarta', 'Asia', 1905000)
insert into country(name, population, capital, continent, area) values('Hungary', 9773000, 'Budapest', 'Europe', 93030)
