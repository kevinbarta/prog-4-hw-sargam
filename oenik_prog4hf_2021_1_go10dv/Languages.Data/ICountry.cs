﻿namespace Languages.Data
{
    /// <summary>
    /// Country.
    /// </summary>
    public interface ICountry
    {
        /// <summary>
        /// Gets or sets country's id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets country's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets country's population.
        /// </summary>
        public int Population { get; set; }

        /// <summary>
        /// Gets or sets country's capital.
        /// </summary>
        public string Capital { get; set; }

        /// <summary>
        /// Gets or sets country's continent.
        /// </summary>
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets country's area.
        /// </summary>
        public int Area { get; set; }
    }
}
