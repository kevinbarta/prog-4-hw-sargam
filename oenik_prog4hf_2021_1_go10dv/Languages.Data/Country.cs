﻿// <copyright file="Country.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Languages.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Country class.
    /// </summary>
    [Table("country")]
    public partial class Country : ICountry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Country"/> class.
        /// </summary>
        public Country()
        {
            this.CountryLangLinks = new HashSet<CountryLangLink>();
        }

        /// <summary>
        /// Gets or sets country id.
        /// </summary>
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets country's name.
        /// </summary>
        [Required]
        [Column("name")]
        [StringLength(40)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets country's population.
        /// </summary>
        [Column("population")]
        public int Population { get; set; }

        /// <summary>
        /// Gets or sets country's capital.
        /// </summary>
        [Required]
        [Column("capital")]
        [StringLength(40)]
        public string Capital { get; set; }

        /// <summary>
        /// Gets or sets country's continent.
        /// </summary>
        [Required]
        [Column("continent")]
        [StringLength(40)]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets country's area.
        /// </summary>
        [Column("area")]
        public int Area { get; set; }

        /// <summary>
        /// Gets or sets country language link.
        /// </summary>
        [InverseProperty(nameof(CountryLangLink.Country))]
        public virtual ICollection<CountryLangLink> CountryLangLinks { get; set; }

        /// <summary>
        /// Copy from another country.
        /// </summary>
        /// <param name="country">The country to copy from.</param>
        /// <returns>The copied instance.</returns>
        public Country CopyFrom(ICountry country)
        {
            typeof(ICountry).GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(country)));
            return this;
        }
    }
}
