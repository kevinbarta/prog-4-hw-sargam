﻿namespace Languages.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Language.
    /// </summary>
    [Table("language")]
    public partial class Language
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Language"/> class.
        /// </summary>
        public Language()
        {
            this.CountryLangLinks = new HashSet<CountryLangLink>();
            this.LangfamLangLinks = new HashSet<LangfamLangLink>();
        }

        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [Key]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        [Required]
        [Column("name")]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Agglutinative.
        /// </summary>
        [Required]
        [Column("agglutinative")]
        [StringLength(1)]
        [IsTorF]
        public string Agglutinative { get; set; }

        /// <summary>
        /// Gets or sets NumberOfTenses.
        /// </summary>
        [Column("number_of_tenses")]
        public int NumberOfTenses { get; set; }

        /// <summary>
        /// Gets or sets NoOfNounDeclensionCases.
        /// </summary>
        [Column("no_of_noun_declension_cases")]
        public int NoOfNounDeclensionCases { get; set; }

        /// <summary>
        /// Gets or sets Difficulty.
        /// </summary>
        [Required]
        [Column("difficulty")]
        [StringLength(20)]
        public string Difficulty { get; set; }

        /// <summary>
        /// Gets or sets NumberOfSpeakers.
        /// </summary>
        [Column("number_of_speakers")]
        public int NumberOfSpeakers { get; set; }

        /// <summary>
        /// Gets or sets RankByNoSpeakers.
        /// </summary>
        [Column("rank_by_no_speakers")]
        public int RankByNoSpeakers { get; set; }

        /// <summary>
        /// Gets or sets CountryLangLinks.
        /// </summary>
        [InverseProperty(nameof(CountryLangLink.Lang))]
        public virtual ICollection<CountryLangLink> CountryLangLinks { get; set; }

        /// <summary>
        /// Gets or sets LangfamLangLinks.
        /// </summary>
        [InverseProperty(nameof(LangfamLangLink.Lang))]
        public virtual ICollection<LangfamLangLink> LangfamLangLinks { get; set; }
    }
}
