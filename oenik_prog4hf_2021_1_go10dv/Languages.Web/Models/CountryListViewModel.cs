﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Languages.Web.Models
{
    /// <summary>
    /// Country list view model.
    /// </summary>
    public class CountryListViewModel
    {
        /// <summary>
        /// Gets or sets edited coutntry.
        /// </summary>
        public Country EditedCountry { get; set; }
        /// <summary>
        /// Gets or sets list of countries.
        /// </summary>
        public List<Country> ListOfCountries { get; set; }
    }
}
