﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Languages.Web.Models
{
    /// <summary>
    /// Country class.
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Gets or sets country id.
        /// </summary>
        [Display(Name = "Country's id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets country's name.
        /// </summary>
        [StringLength(40)]
        [Required]
        [Display(Name = "Country's name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets country's population.
        /// </summary>
        [Display(Name = "Country's population")]
        public int Population { get; set; }

        /// <summary>
        /// Gets or sets country's capital.
        /// </summary>
        [StringLength(40)]
        [Required]
        [Display(Name = "Country's capital")]
        public string Capital { get; set; }

        /// <summary>
        /// Gets or sets country's continent.
        /// </summary>
        [StringLength(40)]
        [Required]
        [Display(Name = "Country's continent")]
        public string Continent { get; set; }

        /// <summary>
        /// Gets or sets country's area.
        /// </summary>
        [Display(Name = "Country's area")]
        public int Area { get; set; }
    }
}
