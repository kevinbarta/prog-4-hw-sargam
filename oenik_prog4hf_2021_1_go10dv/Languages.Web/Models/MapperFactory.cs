﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Languages.Data;

namespace Languages.Web.Models
{
    /// <summary>
    /// Mapper factory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Creates a mapper.
        /// </summary>
        /// <returns>A mapper.</returns>
        public static IMapper CreateMapper()
        {
            MapperConfiguration mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Country, Models.Country>()
                .ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name))
                .ForMember(dest => dest.Population, map => map.MapFrom(src => src.Population))
                .ForMember(dest => dest.Capital, map => map.MapFrom(src => src.Capital))
                .ForMember(dest => dest.Continent, map => map.MapFrom(src => src.Continent))
                .ForMember(dest => dest.Area, map => map.MapFrom(src => src.Area))
                .ReverseMap();
            });

            return mapperConfiguration.CreateMapper();
        }
    }
}
