﻿namespace Languages.Web.Controllers
{
    /// <summary>
    /// API result class.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the operation was succesful.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
