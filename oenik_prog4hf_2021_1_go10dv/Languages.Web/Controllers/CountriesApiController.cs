﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Languages.Data;
using Languages.Logic;
using Languages.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Languages.Web.Controllers
{
    /// <summary>
    /// Countries API controller.
    /// </summary>
    public partial class CountriesApiController : Controller
    {
        private ILogic<Data.Country> logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountriesApiController"/> class.
        /// </summary>
        /// <param name="logic">Country logic.</param>
        /// <param name="mapper">Mapper.</param>
        public CountriesApiController(ILogic<Data.Country> logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Index page.
        /// </summary>
        /// <returns>A view.</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// GET CountriesApi/all
        /// </summary>
        /// <returns>All the countries..</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Country> GetAll()
        {
            var countries = logic.GetAll();
            return mapper.Map<IEnumerable<Data.Country>, List<Models.Country>>(countries);
        }

        /// <summary>
        /// GET: CountriesApi/del
        /// </summary>
        /// <param name="id">Id of the country to be deleted.</param>
        /// <returns>Whether the operation succeeded.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneCountry(int id)
        {
            bool success = true;
            try
            {
                logic.Remove(id);
            }
            catch (Exception)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// POST: CountriesApi/add
        /// </summary>
        /// <param name="country">The country to be added.</param>
        /// <returns>Whether the operation succeeded.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneCountry(Models.Country country)
        {
            bool success = true;
            try
            {
                logic.Insert(mapper.Map<Models.Country, Data.Country>(country));
            }
            catch (Exception)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// POST: CountriesApi/mod
        /// </summary>
        /// <param name="country">The country to be edited.</param>
        /// <returns>Whether the operation succeeded.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCountry(Models.Country country)
        {
            bool success = true;
            try
            {
                logic.Modify(country.Id, mapper.Map<Models.Country, Data.Country>(country));
            }
            catch (Exception)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }
    }
}
