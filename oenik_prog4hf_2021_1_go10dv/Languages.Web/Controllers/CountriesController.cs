﻿using AutoMapper;
using Languages.Data;
using Languages.Logic;
using Languages.Logic.Logics;
using Languages.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Languages.Web.Controllers
{
    /// <summary>
    /// Countries controller.
    /// </summary>
    public class CountriesController : Controller
    {
        private ILogic<Data.Country> logic;
        private IMapper mapper;
        private CountryListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountriesController"/> class.
        /// </summary>
        /// <param name="logic">Country logic.</param>
        /// <param name="mapper">Automapper.</param>
        public CountriesController(ILogic<Data.Country> logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            vm = new CountryListViewModel();
            vm.EditedCountry = new Models.Country();

            var countries = logic.GetAll().ToList();
            vm.ListOfCountries = mapper.Map<IList<Data.Country>, List<Models.Country>>(countries);
        }

        private Models.Country GetCountryModel(int id)
        {
            Data.Country oneCountry = logic.GetOne(id);
            return mapper.Map<Data.Country, Models.Country>(oneCountry);
        }

        /// <summary>
        /// GET: Countries
        /// Main page of the countries.
        /// </summary>
        /// <returns>View.</returns>
        public IActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CountriesIndex", vm);
        }

        /// <summary>
        /// Details of a country.
        /// </summary>
        /// <param name="id">Id of the country.</param>
        /// <returns>View.</returns>
        public IActionResult Details(int id)
        {
            return View("CountriesDetails", GetCountryModel(id));
        }

        /// <summary>
        /// GET: Countries/Remove/1
        /// Remove a country.
        /// </summary>
        /// <param name="id">Id of the country.</param>
        /// <returns>View.</returns>
        public IActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete OK";

            try
            {
                logic.Remove(id);
            }
            catch (Exception)
            {
                TempData["editResult"] = "Delete FAIL";
            }

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Edit a country.
        /// GET: Countries/Edit/1
        /// </summary>
        /// <param name="id">Id of the country.</param>
        /// <returns>View.</returns>
        public IActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedCountry = GetCountryModel(id);
            return View("CountriesIndex", vm);
        }

        /// <summary>
        /// Edit a country.
        /// POST: Countries/Edit + edit action
        /// </summary>
        /// <param name="country">Country.</param>
        /// <param name="editAction">Editing or adding.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Country country, string editAction)
        {
            if (ModelState.IsValid && country != null)
            {
                Data.Country countryData = mapper.Map<Models.Country, Data.Country>(country);

                if (editAction == "AddNew")
                {
                    try
                    {
                        logic.Insert(countryData);
                        TempData["editResult"] = "Edit OK";
                    }
                    catch (Exception)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                else
                {
                    try
                    {

                        logic.Modify(country.Id, countryData);
                        TempData["editResult"] = "Edit OK";
                    }
                    catch (Exception)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedCountry = country;
                return View("CountriesIndex", vm);
            }
        }
    }
}
