﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Languages.WpfClient
{
    /// <summary>
    /// Main view model.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private CountryVM selectedCountry;
        private ObservableCollection<CountryVM> allCountries;

        /// <summary>
        /// Gets or sets selected country.
        /// </summary>
        public CountryVM SelectedCountry
        {
            get { return selectedCountry; }
            set { Set(ref selectedCountry, value); }
        }

        /// <summary>
        /// Gets or sets all countries.
        /// </summary>
        public ObservableCollection<CountryVM> AllCountries
        {
            get { return allCountries; }
            set { Set(ref allCountries, value); }
        }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets edit command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }

        /// <summary>
        /// Gets or sets the editor function.
        /// </summary>
        public Func<CountryVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            logic = new MainLogic();

            AddCmd = new RelayCommand(() => logic.EditCountry(null, EditorFunc));
            DelCmd = new RelayCommand(() => logic.ApiDelCountry(selectedCountry));
            ModCmd = new RelayCommand(() => logic.EditCountry(selectedCountry, EditorFunc));
            LoadCmd = new RelayCommand(() => AllCountries = new ObservableCollection<CountryVM>(logic.ApiGetCountries()));
        }
    }
}
