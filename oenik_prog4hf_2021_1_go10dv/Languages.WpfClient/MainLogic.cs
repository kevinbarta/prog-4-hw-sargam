﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Languages.WpfClient
{
    /// <summary>
    /// The main logic.
    /// </summary>
    public class MainLogic
    {
        private string url = "https://localhost:44360/CountriesApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        private void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully." : "Operation failed.";
            Messenger.Default.Send(msg, "CountryResult");
        }

        /// <summary>
        /// Gets all the countries using the API.
        /// </summary>
        /// <returns>List of the countries.</returns>
        public List<CountryVM> ApiGetCountries()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonSerializer.Deserialize<List<CountryVM>>(json, jsonOptions);
            return list;
        }

        /// <summary>
        /// Delete a country using the API.
        /// </summary>
        /// <param name="country">The country to be deleted.</param>
        public void ApiDelCountry(CountryVM country)
        {
            bool success = false;

            if (country != null)
            {
                string json = client.GetStringAsync(url + "del/" + country.Id).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        private bool ApiEditCountry(CountryVM country, bool isEditing)
        {
            if (country == null)
            {
                return false;
            }

            string fullUrl = url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();

            if (isEditing)
            {
                postData.Add("id", country.Id.ToString());
            }

            postData.Add("name", country.Name);
            postData.Add("population", country.Population.ToString());
            postData.Add("capital", country.Capital);
            postData.Add("continent", country.Continent);
            postData.Add("area", country.Area.ToString());

            string json = client.PostAsync(fullUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edit a country using the API.
        /// </summary>
        /// <param name="country">Country to be edited.</param>
        /// <param name="editorFunc">Calls the editing dialog.</param>
        public void EditCountry(CountryVM country, Func<CountryVM, bool> editorFunc)
        {
            CountryVM clone = new CountryVM();

            if (country != null)
            {
                clone.CopyFrom(country);
            }

            bool? success = editorFunc?.Invoke(clone);

            if (success == true)
            {
                if (country != null)
                {
                    success = ApiEditCountry(clone, true);
                }
                else
                {
                    success = ApiEditCountry(clone, false);
                }
            }

            SendMessage(success == true);
        }
    }
}
