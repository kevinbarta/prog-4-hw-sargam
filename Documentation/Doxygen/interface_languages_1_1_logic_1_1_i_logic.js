var interface_languages_1_1_logic_1_1_i_logic =
[
    [ "GetAll", "interface_languages_1_1_logic_1_1_i_logic.html#aa41d4f69395ecd3f4a103754617a9c65", null ],
    [ "GetOne", "interface_languages_1_1_logic_1_1_i_logic.html#ab8bec272d5c021923880e17b51d80c71", null ],
    [ "Insert", "interface_languages_1_1_logic_1_1_i_logic.html#a4fc3de03a0edbfcc999086534cd64b34", null ],
    [ "Modify", "interface_languages_1_1_logic_1_1_i_logic.html#a381caf76d140ca94808daa126f71231a", null ],
    [ "Modify", "interface_languages_1_1_logic_1_1_i_logic.html#a1dd3a35e0f0cca1f5a8484e942e512c4", null ],
    [ "Remove", "interface_languages_1_1_logic_1_1_i_logic.html#ad30849fe9041927756a7e23df8b0574b", null ]
];