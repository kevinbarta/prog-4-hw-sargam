var namespace_languages_1_1_logic_1_1_logics =
[
    [ "CountryLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic" ],
    [ "CountryLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_country_logic" ],
    [ "LangfamLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic" ],
    [ "LanguageFamilyLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic" ],
    [ "LanguageLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_language_logic" ]
];