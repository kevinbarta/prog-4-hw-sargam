var class_languages_1_1_logic_1_1_logics_1_1_language_family_logic =
[
    [ "LanguageFamilyLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#a3cbb6640cfb03eb17f00a0ddd2b908d5", null ],
    [ "LanguageFamilyLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#a30d8a516ee6474d2271ec68708a8c5bc", null ],
    [ "GetAll", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#acc02ab3d10cef29db7ef0fabc8bada82", null ],
    [ "GetOne", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#a8c37606184ad5af6ffafbf54e45e8692", null ],
    [ "Insert", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#ac2992d812b79f87c5fc4726da5550bb8", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#a17ac7ac728e946332a0cc24f8c6fe7ca", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#a0067c4d028ba5f3840552772649842e9", null ],
    [ "Remove", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html#a7111e5e78dff960b6f18f47ef92caa8d", null ]
];