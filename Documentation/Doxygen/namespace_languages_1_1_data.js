var namespace_languages_1_1_data =
[
    [ "Country", "class_languages_1_1_data_1_1_country.html", "class_languages_1_1_data_1_1_country" ],
    [ "CountryLangLink", "class_languages_1_1_data_1_1_country_lang_link.html", "class_languages_1_1_data_1_1_country_lang_link" ],
    [ "DatabaseEntities", "class_languages_1_1_data_1_1_database_entities.html", "class_languages_1_1_data_1_1_database_entities" ],
    [ "ICountry", "interface_languages_1_1_data_1_1_i_country.html", "interface_languages_1_1_data_1_1_i_country" ],
    [ "IsTorF", "class_languages_1_1_data_1_1_is_tor_f.html", "class_languages_1_1_data_1_1_is_tor_f" ],
    [ "LangfamLangLink", "class_languages_1_1_data_1_1_langfam_lang_link.html", "class_languages_1_1_data_1_1_langfam_lang_link" ],
    [ "Language", "class_languages_1_1_data_1_1_language.html", "class_languages_1_1_data_1_1_language" ],
    [ "LanguageFamily", "class_languages_1_1_data_1_1_language_family.html", "class_languages_1_1_data_1_1_language_family" ]
];