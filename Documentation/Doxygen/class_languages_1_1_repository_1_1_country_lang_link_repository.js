var class_languages_1_1_repository_1_1_country_lang_link_repository =
[
    [ "CountryLangLinkRepository", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#a2f51ce78ffd49428f3601c757af32565", null ],
    [ "GetAll", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#a313f6d963401f35a66acb083c6f58beb", null ],
    [ "GetOne", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#a55cb9e8867619d87909cc2aa2ad36b5f", null ],
    [ "Insert", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#ae001703b6b2cae7853f9a8aac9a68682", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#a3c901799b186439e1fa106548ce7dcd3", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#a433c29c699e915b776254734ead03db0", null ],
    [ "Remove", "class_languages_1_1_repository_1_1_country_lang_link_repository.html#af637c2c096f1f3d76530f185d9f46a30", null ]
];