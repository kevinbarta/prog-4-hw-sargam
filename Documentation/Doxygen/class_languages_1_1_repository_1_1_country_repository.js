var class_languages_1_1_repository_1_1_country_repository =
[
    [ "CountryRepository", "class_languages_1_1_repository_1_1_country_repository.html#adf76696a58e107394abdec44552322bd", null ],
    [ "GetAll", "class_languages_1_1_repository_1_1_country_repository.html#a45296ad6f585bf3364117340e043d6fa", null ],
    [ "GetOne", "class_languages_1_1_repository_1_1_country_repository.html#a2bb3f718f08e9e704e022c8f77a24f26", null ],
    [ "Insert", "class_languages_1_1_repository_1_1_country_repository.html#a9d944fa5e01b2401ce35f5bac3be3abc", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_country_repository.html#a67dfeabc159de515cd1ba784311274e4", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_country_repository.html#a561739de82e6b0fd3a49a50d98ca354f", null ],
    [ "Remove", "class_languages_1_1_repository_1_1_country_repository.html#a1c84e96a1a98c043e46d46cfe37da51a", null ]
];