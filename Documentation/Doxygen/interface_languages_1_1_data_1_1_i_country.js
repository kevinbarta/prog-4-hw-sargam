var interface_languages_1_1_data_1_1_i_country =
[
    [ "Area", "interface_languages_1_1_data_1_1_i_country.html#a892e0a6a629d900e649d737a3d2f0a4a", null ],
    [ "Capital", "interface_languages_1_1_data_1_1_i_country.html#ae68da9176ea7fbce2c33dd49c6603e0d", null ],
    [ "Continent", "interface_languages_1_1_data_1_1_i_country.html#a6c005ed22e6a3492e130dc4219b54a7f", null ],
    [ "Id", "interface_languages_1_1_data_1_1_i_country.html#ac9e52f2a5e41a323585f2294f09fe5af", null ],
    [ "Name", "interface_languages_1_1_data_1_1_i_country.html#afff764fc13ab69d67e7ddb13e658118f", null ],
    [ "Population", "interface_languages_1_1_data_1_1_i_country.html#a7768a193f628390c199cafe5403fcf9a", null ]
];