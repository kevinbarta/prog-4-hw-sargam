var searchData=
[
  ['databaseentities_290',['DatabaseEntities',['../class_languages_1_1_data_1_1_database_entities.html#aec78a0fc6aa1dd912194cf278afc4b99',1,'Languages.Data.DatabaseEntities.DatabaseEntities()'],['../class_languages_1_1_data_1_1_database_entities.html#a5bc2f27b4749b17d2367cd048670a8bb',1,'Languages.Data.DatabaseEntities.DatabaseEntities(DbContextOptions&lt; DatabaseEntities &gt; options)']]],
  ['deletecountry_291',['DeleteCountry',['../class_languages_1_1_w_p_f_1_1_b_l_1_1_country_logic_v_m.html#a5180a4b986c6dde4557f42c82ecd1448',1,'Languages.WPF.BL.CountryLogicVM.DeleteCountry()'],['../interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_country_logic_v_m.html#a7f9ae3fa5a90a6290cb716e3a5494aa0',1,'Languages.WPF.BL.ICountryLogicVM.DeleteCountry()']]],
  ['delonecountry_292',['DelOneCountry',['../class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a36f0650df73d787a7aa81ea41441057f',1,'Languages::Web::Controllers::CountriesApiController']]],
  ['details_293',['Details',['../class_languages_1_1_web_1_1_controllers_1_1_countries_controller.html#afbaf06004aecceac670f34b052498724',1,'Languages::Web::Controllers::CountriesController']]]
];
