var searchData=
[
  ['qlanguagefamilies_149',['QLanguageFamilies',['../class_languages_1_1_logic_1_1_q_language_families.html',1,'Languages.Logic.QLanguageFamilies'],['../class_languages_1_1_logic_1_1_q_language_families.html#ac3228d3604673c907120442fd2b390a8',1,'Languages.Logic.QLanguageFamilies.QLanguageFamilies()']]],
  ['qlanguagesbydifficulty_150',['QLanguagesByDifficulty',['../class_languages_1_1_logic_1_1_q_languages_by_difficulty.html',1,'Languages.Logic.QLanguagesByDifficulty'],['../class_languages_1_1_logic_1_1_q_languages_by_difficulty.html#a24ab15a806a569be2ad0d94df0bcfff9',1,'Languages.Logic.QLanguagesByDifficulty.QLanguagesByDifficulty()']]],
  ['qnumberofspeakers_151',['QNumberOfSpeakers',['../class_languages_1_1_logic_1_1_q_number_of_speakers.html',1,'Languages.Logic.QNumberOfSpeakers'],['../class_languages_1_1_logic_1_1_q_number_of_speakers.html#a0d2f409206584f8bcee890d61eeb7bdb',1,'Languages.Logic.QNumberOfSpeakers.QNumberOfSpeakers()']]],
  ['qofficiallanguages_152',['QOfficialLanguages',['../class_languages_1_1_logic_1_1_q_official_languages.html',1,'Languages.Logic.QOfficialLanguages'],['../class_languages_1_1_logic_1_1_q_official_languages.html#a4f7b6bb8aa80fdf08d368c37a214f438',1,'Languages.Logic.QOfficialLanguages.QOfficialLanguages()']]]
];
