var searchData=
[
  ['db_355',['DB',['../class_languages_1_1_logic_1_1_logic_base.html#a6bc56b110f0fdb08330aff20ac656e24',1,'Languages::Logic::LogicBase']]],
  ['delcmd_356',['DelCmd',['../class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#aca6e68efdaf0c9e99e1ad4bb119ae576',1,'Languages.WPF.VM.MainViewModel.DelCmd()'],['../class_languages_1_1_wpf_client_1_1_main_v_m.html#a0e6f9d4f6c6d6e5c2930b461e41d4094',1,'Languages.WpfClient.MainVM.DelCmd()']]],
  ['difficulty_357',['Difficulty',['../class_languages_1_1_data_1_1_language.html#a35abcade9a181de645f9dc2d50479284',1,'Languages.Data.Language.Difficulty()'],['../class_languages_1_1_logic_1_1_q_languages_by_difficulty.html#a74ae8b527ae667e195ed426d18a6bcee',1,'Languages.Logic.QLanguagesByDifficulty.Difficulty()'],['../class_languages_1_1_logic_1_1_q_number_of_speakers.html#acacdd25b8430b54884c7c9b91c2de51e',1,'Languages.Logic.QNumberOfSpeakers.Difficulty()']]]
];
