var class_languages_1_1_wpf_client_1_1_main_v_m =
[
    [ "MainVM", "class_languages_1_1_wpf_client_1_1_main_v_m.html#a27e5fa7035ab79e23569388f70bd90f4", null ],
    [ "AddCmd", "class_languages_1_1_wpf_client_1_1_main_v_m.html#ab39e544af76888c4517b3e98699ff94c", null ],
    [ "AllCountries", "class_languages_1_1_wpf_client_1_1_main_v_m.html#aa2b27b7cb4c5dbe4cc98125a74a3d4de", null ],
    [ "DelCmd", "class_languages_1_1_wpf_client_1_1_main_v_m.html#a0e6f9d4f6c6d6e5c2930b461e41d4094", null ],
    [ "EditorFunc", "class_languages_1_1_wpf_client_1_1_main_v_m.html#a441096a7452211acffaba162b968813e", null ],
    [ "LoadCmd", "class_languages_1_1_wpf_client_1_1_main_v_m.html#ae00580739e89966fe39bfa40edd77ff6", null ],
    [ "ModCmd", "class_languages_1_1_wpf_client_1_1_main_v_m.html#a50a08bb164396f03008ff6a724ab7e84", null ],
    [ "SelectedCountry", "class_languages_1_1_wpf_client_1_1_main_v_m.html#af1d586006d555172c5f500679a682a73", null ]
];