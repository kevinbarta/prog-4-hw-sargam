var class_languages_1_1_data_1_1_language_family =
[
    [ "LanguageFamily", "class_languages_1_1_data_1_1_language_family.html#a14307ad0ab63ad051eacca6ce2e32c34", null ],
    [ "Id", "class_languages_1_1_data_1_1_language_family.html#a4c84b02af1dbab708c7b894e8517d6a9", null ],
    [ "IsoCode", "class_languages_1_1_data_1_1_language_family.html#a7894d1a76387ce54b80ac3f588c00c2a", null ],
    [ "LangfamLangLinks", "class_languages_1_1_data_1_1_language_family.html#ad2121f9615fc7d22f9c4461311235325", null ],
    [ "Name", "class_languages_1_1_data_1_1_language_family.html#a5eaf2e8242e0edb49ee5e7097288b422", null ],
    [ "NumberOfLanguages", "class_languages_1_1_data_1_1_language_family.html#aa534168029ee342f34c29b441eef8f8b", null ],
    [ "NumberOfSpeakers", "class_languages_1_1_data_1_1_language_family.html#a58cee4e5b220932fca8d83feed87ab47", null ],
    [ "RankByNoLanguages", "class_languages_1_1_data_1_1_language_family.html#a5ff56f0e1f2644678efb699f2c35a635", null ],
    [ "RankByNoSpeakers", "class_languages_1_1_data_1_1_language_family.html#a2b88c42e3ad1c304815061f843dbfa98", null ]
];