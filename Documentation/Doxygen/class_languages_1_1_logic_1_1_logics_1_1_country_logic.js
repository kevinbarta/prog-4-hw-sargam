var class_languages_1_1_logic_1_1_logics_1_1_country_logic =
[
    [ "CountryLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#a816f5041fe7f35d121ae188922bd9749", null ],
    [ "CountryLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#a646949a0c5fae7b6aa12d712b7cfa1f7", null ],
    [ "GetAll", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#a93ede1264655d914dfe342d969d07f17", null ],
    [ "GetOne", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#a219eae46f0c362d53e79dffd193a998e", null ],
    [ "Insert", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#a94fdf57cc8829b59809f528b9e0800fb", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#ace06f5a32f34a18a82c02d4bbb60afa2", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#ad67b7ef0847771e0322242c37721855a", null ],
    [ "Remove", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html#a9f8c2f9b3909c5dfec9647a24f8e2b58", null ]
];