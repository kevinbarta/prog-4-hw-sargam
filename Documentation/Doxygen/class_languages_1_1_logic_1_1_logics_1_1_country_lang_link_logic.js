var class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic =
[
    [ "CountryLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#a451387b3347b475385aaff62f67a0c90", null ],
    [ "CountryLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#af68132101cf6105ac005b450fe4ad485", null ],
    [ "GetAll", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#a738c0594c09c4f17228204c411fbd35a", null ],
    [ "GetOne", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#a05377b36424aa39f473c64827ee27d7e", null ],
    [ "Insert", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#a16d6aa7fb22904a0397c81c498141f63", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#a808a4ce00957d646daa4030603f44bed", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#a8e7dbbe9c026e0aa98c9e99ea1679d08", null ],
    [ "Remove", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html#ab5548a17923a97267fc74af4ba473ee6", null ]
];