var hierarchy =
[
    [ "Languages.WPF.Data.AllContinents", "class_languages_1_1_w_p_f_1_1_data_1_1_all_continents.html", null ],
    [ "Languages.WpfClient.AllContinents", "class_languages_1_1_wpf_client_1_1_all_continents.html", null ],
    [ "Languages.Web.Controllers.CountriesApiController.ApiResult", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "Languages.WPF.App", "class_languages_1_1_w_p_f_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "Languages.WPF.App", "class_languages_1_1_w_p_f_1_1_app.html", null ],
      [ "Languages.WPF.App", "class_languages_1_1_w_p_f_1_1_app.html", null ],
      [ "Languages.WPF.App", "class_languages_1_1_w_p_f_1_1_app.html", null ],
      [ "Languages.WPF.App", "class_languages_1_1_w_p_f_1_1_app.html", null ],
      [ "Languages.WpfClient.App", "class_languages_1_1_wpf_client_1_1_app.html", null ],
      [ "Languages.WpfClient.App", "class_languages_1_1_wpf_client_1_1_app.html", null ],
      [ "Languages.WpfClient.App", "class_languages_1_1_wpf_client_1_1_app.html", null ],
      [ "Languages.WpfClient.App", "class_languages_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "Controller", null, [
      [ "Languages.Web.Controllers.CountriesApiController", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html", null ],
      [ "Languages.Web.Controllers.CountriesController", "class_languages_1_1_web_1_1_controllers_1_1_countries_controller.html", null ],
      [ "Languages.Web.Controllers.HomeController", "class_languages_1_1_web_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "Languages.Web.Models.Country", "class_languages_1_1_web_1_1_models_1_1_country.html", null ],
    [ "Languages.Data.CountryLangLink", "class_languages_1_1_data_1_1_country_lang_link.html", null ],
    [ "Languages.Web.Models.CountryListViewModel", "class_languages_1_1_web_1_1_models_1_1_country_list_view_model.html", null ],
    [ "DbContext", null, [
      [ "Languages.Data.DatabaseEntities", "class_languages_1_1_data_1_1_database_entities.html", null ]
    ] ],
    [ "Languages.Web.Models.ErrorViewModel", "class_languages_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "FormatException", null, [
      [ "Languages.Repository.EmptyInputException", "class_languages_1_1_repository_1_1_empty_input_exception.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.UI.EditorWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WPF.UI.EditorWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.UI.EditorWindow", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WpfClient.UI.EditorWindow", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "Languages.Data.ICountry", "interface_languages_1_1_data_1_1_i_country.html", [
      [ "Languages.Data.Country", "class_languages_1_1_data_1_1_country.html", null ],
      [ "Languages.WPF.Data.CountryVM", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html", null ],
      [ "Languages.WpfClient.CountryVM", "class_languages_1_1_wpf_client_1_1_country_v_m.html", null ]
    ] ],
    [ "Languages.WPF.BL.ICountryLogicVM", "interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_country_logic_v_m.html", [
      [ "Languages.WPF.BL.CountryLogicVM", "class_languages_1_1_w_p_f_1_1_b_l_1_1_country_logic_v_m.html", null ]
    ] ],
    [ "Languages.WPF.BL.IEditorService", "interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", [
      [ "Languages.WPF.UI.EditorServiceViaWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "Languages.Logic.QLanguageFamilies", "class_languages_1_1_logic_1_1_q_language_families.html", null ],
      [ "Languages.Logic.QLanguagesByDifficulty", "class_languages_1_1_logic_1_1_q_languages_by_difficulty.html", null ]
    ] ],
    [ "Languages.Logic.ILanguageLogic", "interface_languages_1_1_logic_1_1_i_language_logic.html", [
      [ "Languages.Logic.Logics.LanguageLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html", null ]
    ] ],
    [ "Languages.Logic.ILogic< T >", "interface_languages_1_1_logic_1_1_i_logic.html", null ],
    [ "Languages.Logic.ILogic< Country >", "interface_languages_1_1_logic_1_1_i_logic.html", [
      [ "Languages.Logic.Logics.CountryLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html", null ]
    ] ],
    [ "Languages.Logic.ILogic< CountryLangLink >", "interface_languages_1_1_logic_1_1_i_logic.html", [
      [ "Languages.Logic.Logics.CountryLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html", null ]
    ] ],
    [ "Languages.Logic.ILogic< Data.Country >", "interface_languages_1_1_logic_1_1_i_logic.html", null ],
    [ "Languages.Logic.ILogic< LangfamLangLink >", "interface_languages_1_1_logic_1_1_i_logic.html", [
      [ "Languages.Logic.Logics.LangfamLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html", null ]
    ] ],
    [ "Languages.Logic.ILogic< Language >", "interface_languages_1_1_logic_1_1_i_logic.html", [
      [ "Languages.Logic.Logics.LanguageLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html", null ]
    ] ],
    [ "Languages.Logic.ILogic< LanguageFamily >", "interface_languages_1_1_logic_1_1_i_logic.html", [
      [ "Languages.Logic.Logics.LanguageFamilyLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Languages.Repository.IRepository< T >", "interface_languages_1_1_repository_1_1_i_repository.html", null ],
    [ "Languages.Repository.IRepository< Country >", "interface_languages_1_1_repository_1_1_i_repository.html", [
      [ "Languages.Repository.CountryRepository", "class_languages_1_1_repository_1_1_country_repository.html", null ]
    ] ],
    [ "Languages.Repository.IRepository< CountryLangLink >", "interface_languages_1_1_repository_1_1_i_repository.html", [
      [ "Languages.Repository.CountryLangLinkRepository", "class_languages_1_1_repository_1_1_country_lang_link_repository.html", null ]
    ] ],
    [ "Languages.Repository.IRepository< LangfamLangLink >", "interface_languages_1_1_repository_1_1_i_repository.html", [
      [ "Languages.Repository.LangfamLangLinkRepository", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html", null ]
    ] ],
    [ "Languages.Repository.IRepository< Language >", "interface_languages_1_1_repository_1_1_i_repository.html", [
      [ "Languages.Repository.LanguageRepository", "class_languages_1_1_repository_1_1_language_repository.html", null ]
    ] ],
    [ "Languages.Repository.IRepository< LanguageFamily >", "interface_languages_1_1_repository_1_1_i_repository.html", [
      [ "Languages.Repository.LanguageFamilyRepository", "class_languages_1_1_repository_1_1_language_family_repository.html", null ]
    ] ],
    [ "Languages.Repository.IRepository< Languages.Data.Country >", "interface_languages_1_1_repository_1_1_i_repository.html", null ],
    [ "Languages.Repository.IRepository< Languages.Data.CountryLangLink >", "interface_languages_1_1_repository_1_1_i_repository.html", null ],
    [ "Languages.Repository.IRepository< Languages.Data.LangfamLangLink >", "interface_languages_1_1_repository_1_1_i_repository.html", null ],
    [ "Languages.Repository.IRepository< Languages.Data.Language >", "interface_languages_1_1_repository_1_1_i_repository.html", null ],
    [ "Languages.Repository.IRepository< Languages.Data.LanguageFamily >", "interface_languages_1_1_repository_1_1_i_repository.html", null ],
    [ "Languages.Data.LangfamLangLink", "class_languages_1_1_data_1_1_langfam_lang_link.html", null ],
    [ "Languages.Data.Language", "class_languages_1_1_data_1_1_language.html", null ],
    [ "Languages.Data.LanguageFamily", "class_languages_1_1_data_1_1_language_family.html", null ],
    [ "Languages.Logic.LogicBase", "class_languages_1_1_logic_1_1_logic_base.html", null ],
    [ "Languages.WpfClient.MainLogic", "class_languages_1_1_wpf_client_1_1_main_logic.html", null ],
    [ "Languages.Web.Models.MapperFactory", "class_languages_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "Languages.WPF.Data.CountryVM", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html", null ],
      [ "Languages.WpfClient.CountryVM", "class_languages_1_1_wpf_client_1_1_country_v_m.html", null ]
    ] ],
    [ "Languages.Web.Program", "class_languages_1_1_web_1_1_program.html", null ],
    [ "Languages.Logic.QNumberOfSpeakers", "class_languages_1_1_logic_1_1_q_number_of_speakers.html", null ],
    [ "Languages.Logic.QOfficialLanguages", "class_languages_1_1_logic_1_1_q_official_languages.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Countries_CountriesDetails", "class_asp_net_core_1_1_views___countries___countries_details.html", null ],
      [ "AspNetCore.Views_Countries_CountriesEdit", "class_asp_net_core_1_1_views___countries___countries_edit.html", null ],
      [ "AspNetCore.Views_Countries_CountriesIndex", "class_asp_net_core_1_1_views___countries___countries_index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Languages.Web.Models.Country >>", null, [
      [ "AspNetCore.Views_Countries_CountriesList", "class_asp_net_core_1_1_views___countries___countries_list.html", null ]
    ] ],
    [ "Languages.Web.Startup", "class_languages_1_1_web_1_1_startup.html", null ],
    [ "ValidationAttribute", null, [
      [ "Languages.Data.IsTorF", "class_languages_1_1_data_1_1_is_tor_f.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "Languages.WPF.VM.EditorViewModel", "class_languages_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "Languages.WPF.VM.MainViewModel", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", null ],
      [ "Languages.WpfClient.MainVM", "class_languages_1_1_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Languages.WPF.UI.EditorWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WPF.UI.EditorWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WPF.UI.EditorWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Languages.WpfClient.UI.EditorWindow", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WpfClient.UI.EditorWindow", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window.html", null ],
      [ "Languages.WpfClient.UI.EditorWindow", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Languages.WPF.MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", null ]
    ] ]
];