var namespace_languages_1_1_web =
[
    [ "Controllers", "namespace_languages_1_1_web_1_1_controllers.html", "namespace_languages_1_1_web_1_1_controllers" ],
    [ "Models", "namespace_languages_1_1_web_1_1_models.html", "namespace_languages_1_1_web_1_1_models" ],
    [ "Program", "class_languages_1_1_web_1_1_program.html", "class_languages_1_1_web_1_1_program" ],
    [ "Startup", "class_languages_1_1_web_1_1_startup.html", "class_languages_1_1_web_1_1_startup" ]
];