var class_languages_1_1_repository_1_1_language_repository =
[
    [ "LanguageRepository", "class_languages_1_1_repository_1_1_language_repository.html#a0c9bd4bd592b3597a30df7e7f8f4582c", null ],
    [ "GetAll", "class_languages_1_1_repository_1_1_language_repository.html#a3bd203ce546bbe2adf1f1d9565cb163d", null ],
    [ "GetOne", "class_languages_1_1_repository_1_1_language_repository.html#a69bebb4f150fca50f74dd1bce760e1f2", null ],
    [ "Insert", "class_languages_1_1_repository_1_1_language_repository.html#aded238b41ca9df3752a20f32376e9ac4", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_language_repository.html#adcd9f16b8e4484037860ef50ad93a83f", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_language_repository.html#af53bea780679bdad902581423e525a67", null ],
    [ "Remove", "class_languages_1_1_repository_1_1_language_repository.html#a1306ad307b5ddf4b1c9470f4067646a7", null ]
];