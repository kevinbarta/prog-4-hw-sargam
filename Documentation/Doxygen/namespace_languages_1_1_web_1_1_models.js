var namespace_languages_1_1_web_1_1_models =
[
    [ "Country", "class_languages_1_1_web_1_1_models_1_1_country.html", "class_languages_1_1_web_1_1_models_1_1_country" ],
    [ "CountryListViewModel", "class_languages_1_1_web_1_1_models_1_1_country_list_view_model.html", "class_languages_1_1_web_1_1_models_1_1_country_list_view_model" ],
    [ "ErrorViewModel", "class_languages_1_1_web_1_1_models_1_1_error_view_model.html", "class_languages_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_languages_1_1_web_1_1_models_1_1_mapper_factory.html", "class_languages_1_1_web_1_1_models_1_1_mapper_factory" ]
];