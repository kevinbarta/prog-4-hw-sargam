var class_languages_1_1_repository_1_1_langfam_lang_link_repository =
[
    [ "LangfamLangLinkRepository", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#ae0b6d59adcda200475eb33875ae68088", null ],
    [ "GetAll", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#a7b24b9a74886389b799df88a316d01aa", null ],
    [ "GetOne", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#a2652a73b828fccb16a6e98b7cee98a2d", null ],
    [ "Insert", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#a3a573c4ceb0b3d23661f7183985d9b58", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#ac6e1440fe1495e5f92c3c6ae9932048e", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#ae41a3ed67c5ab3cab5a96458a6b4bf94", null ],
    [ "Remove", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html#a6e2e5bf1a2462d961b8c760100fdf85c", null ]
];