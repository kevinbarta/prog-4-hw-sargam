var class_languages_1_1_web_1_1_models_1_1_country =
[
    [ "Area", "class_languages_1_1_web_1_1_models_1_1_country.html#ac8a23a2860f12ef0da8fe26a12b4a360", null ],
    [ "Capital", "class_languages_1_1_web_1_1_models_1_1_country.html#a3c875aa22c9109290cf44eb14dba7de0", null ],
    [ "Continent", "class_languages_1_1_web_1_1_models_1_1_country.html#a9551663360641febc6a1da89e5cdeaaf", null ],
    [ "Id", "class_languages_1_1_web_1_1_models_1_1_country.html#a1290d6a7c8163eadc0e31342acd6a63c", null ],
    [ "Name", "class_languages_1_1_web_1_1_models_1_1_country.html#ad43f82b3558cb7bf78af473cf5c6ccf3", null ],
    [ "Population", "class_languages_1_1_web_1_1_models_1_1_country.html#afcfac31a81575eb7ed4d5e672b2246b0", null ]
];