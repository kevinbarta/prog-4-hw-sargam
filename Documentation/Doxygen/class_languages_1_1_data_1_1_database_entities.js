var class_languages_1_1_data_1_1_database_entities =
[
    [ "DatabaseEntities", "class_languages_1_1_data_1_1_database_entities.html#aec78a0fc6aa1dd912194cf278afc4b99", null ],
    [ "DatabaseEntities", "class_languages_1_1_data_1_1_database_entities.html#a5bc2f27b4749b17d2367cd048670a8bb", null ],
    [ "OnConfiguring", "class_languages_1_1_data_1_1_database_entities.html#acdd32848e914440eb13fb901fd07fbf3", null ],
    [ "OnModelCreating", "class_languages_1_1_data_1_1_database_entities.html#a6e6f1fc73ca1472c1aa85b167dc87387", null ],
    [ "Countries", "class_languages_1_1_data_1_1_database_entities.html#afccc4aa0d9ed30d3322d363459d6f081", null ],
    [ "CountryLangLinks", "class_languages_1_1_data_1_1_database_entities.html#a0c7fc99862fff3293910afc059059c10", null ],
    [ "LangfamLangLinks", "class_languages_1_1_data_1_1_database_entities.html#a9f0768bfd11ac35401dfba6f1fd6a216", null ],
    [ "LanguageFamilies", "class_languages_1_1_data_1_1_database_entities.html#a9dce0be2750f968df6c5557c0f5884c3", null ],
    [ "Languages", "class_languages_1_1_data_1_1_database_entities.html#a864a248fd179571262766654cd6c6e61", null ]
];