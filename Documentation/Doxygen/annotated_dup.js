var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Countries_CountriesDetails", "class_asp_net_core_1_1_views___countries___countries_details.html", "class_asp_net_core_1_1_views___countries___countries_details" ],
      [ "Views_Countries_CountriesEdit", "class_asp_net_core_1_1_views___countries___countries_edit.html", "class_asp_net_core_1_1_views___countries___countries_edit" ],
      [ "Views_Countries_CountriesIndex", "class_asp_net_core_1_1_views___countries___countries_index.html", "class_asp_net_core_1_1_views___countries___countries_index" ],
      [ "Views_Countries_CountriesList", "class_asp_net_core_1_1_views___countries___countries_list.html", "class_asp_net_core_1_1_views___countries___countries_list" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "Languages", "namespace_languages.html", [
      [ "Data", "namespace_languages_1_1_data.html", [
        [ "Country", "class_languages_1_1_data_1_1_country.html", "class_languages_1_1_data_1_1_country" ],
        [ "CountryLangLink", "class_languages_1_1_data_1_1_country_lang_link.html", "class_languages_1_1_data_1_1_country_lang_link" ],
        [ "DatabaseEntities", "class_languages_1_1_data_1_1_database_entities.html", "class_languages_1_1_data_1_1_database_entities" ],
        [ "ICountry", "interface_languages_1_1_data_1_1_i_country.html", "interface_languages_1_1_data_1_1_i_country" ],
        [ "IsTorF", "class_languages_1_1_data_1_1_is_tor_f.html", "class_languages_1_1_data_1_1_is_tor_f" ],
        [ "LangfamLangLink", "class_languages_1_1_data_1_1_langfam_lang_link.html", "class_languages_1_1_data_1_1_langfam_lang_link" ],
        [ "Language", "class_languages_1_1_data_1_1_language.html", "class_languages_1_1_data_1_1_language" ],
        [ "LanguageFamily", "class_languages_1_1_data_1_1_language_family.html", "class_languages_1_1_data_1_1_language_family" ]
      ] ],
      [ "Logic", "namespace_languages_1_1_logic.html", [
        [ "Logics", "namespace_languages_1_1_logic_1_1_logics.html", [
          [ "CountryLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_country_lang_link_logic" ],
          [ "CountryLogic", "class_languages_1_1_logic_1_1_logics_1_1_country_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_country_logic" ],
          [ "LangfamLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic" ],
          [ "LanguageFamilyLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_language_family_logic" ],
          [ "LanguageLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html", "class_languages_1_1_logic_1_1_logics_1_1_language_logic" ]
        ] ],
        [ "ILanguageLogic", "interface_languages_1_1_logic_1_1_i_language_logic.html", "interface_languages_1_1_logic_1_1_i_language_logic" ],
        [ "ILogic", "interface_languages_1_1_logic_1_1_i_logic.html", "interface_languages_1_1_logic_1_1_i_logic" ],
        [ "LogicBase", "class_languages_1_1_logic_1_1_logic_base.html", "class_languages_1_1_logic_1_1_logic_base" ],
        [ "QLanguageFamilies", "class_languages_1_1_logic_1_1_q_language_families.html", "class_languages_1_1_logic_1_1_q_language_families" ],
        [ "QLanguagesByDifficulty", "class_languages_1_1_logic_1_1_q_languages_by_difficulty.html", "class_languages_1_1_logic_1_1_q_languages_by_difficulty" ],
        [ "QNumberOfSpeakers", "class_languages_1_1_logic_1_1_q_number_of_speakers.html", "class_languages_1_1_logic_1_1_q_number_of_speakers" ],
        [ "QOfficialLanguages", "class_languages_1_1_logic_1_1_q_official_languages.html", "class_languages_1_1_logic_1_1_q_official_languages" ]
      ] ],
      [ "Repository", "namespace_languages_1_1_repository.html", [
        [ "EmptyInputException", "class_languages_1_1_repository_1_1_empty_input_exception.html", "class_languages_1_1_repository_1_1_empty_input_exception" ],
        [ "IRepository", "interface_languages_1_1_repository_1_1_i_repository.html", "interface_languages_1_1_repository_1_1_i_repository" ],
        [ "CountryLangLinkRepository", "class_languages_1_1_repository_1_1_country_lang_link_repository.html", "class_languages_1_1_repository_1_1_country_lang_link_repository" ],
        [ "CountryRepository", "class_languages_1_1_repository_1_1_country_repository.html", "class_languages_1_1_repository_1_1_country_repository" ],
        [ "LangfamLangLinkRepository", "class_languages_1_1_repository_1_1_langfam_lang_link_repository.html", "class_languages_1_1_repository_1_1_langfam_lang_link_repository" ],
        [ "LanguageFamilyRepository", "class_languages_1_1_repository_1_1_language_family_repository.html", "class_languages_1_1_repository_1_1_language_family_repository" ],
        [ "LanguageRepository", "class_languages_1_1_repository_1_1_language_repository.html", "class_languages_1_1_repository_1_1_language_repository" ]
      ] ],
      [ "Web", "namespace_languages_1_1_web.html", [
        [ "Controllers", "namespace_languages_1_1_web_1_1_controllers.html", [
          [ "CountriesApiController", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller" ],
          [ "CountriesController", "class_languages_1_1_web_1_1_controllers_1_1_countries_controller.html", "class_languages_1_1_web_1_1_controllers_1_1_countries_controller" ],
          [ "HomeController", "class_languages_1_1_web_1_1_controllers_1_1_home_controller.html", "class_languages_1_1_web_1_1_controllers_1_1_home_controller" ]
        ] ],
        [ "Models", "namespace_languages_1_1_web_1_1_models.html", [
          [ "Country", "class_languages_1_1_web_1_1_models_1_1_country.html", "class_languages_1_1_web_1_1_models_1_1_country" ],
          [ "CountryListViewModel", "class_languages_1_1_web_1_1_models_1_1_country_list_view_model.html", "class_languages_1_1_web_1_1_models_1_1_country_list_view_model" ],
          [ "ErrorViewModel", "class_languages_1_1_web_1_1_models_1_1_error_view_model.html", "class_languages_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_languages_1_1_web_1_1_models_1_1_mapper_factory.html", "class_languages_1_1_web_1_1_models_1_1_mapper_factory" ]
        ] ],
        [ "Program", "class_languages_1_1_web_1_1_program.html", "class_languages_1_1_web_1_1_program" ],
        [ "Startup", "class_languages_1_1_web_1_1_startup.html", "class_languages_1_1_web_1_1_startup" ]
      ] ],
      [ "WPF", "namespace_languages_1_1_w_p_f.html", [
        [ "BL", "namespace_languages_1_1_w_p_f_1_1_b_l.html", [
          [ "CountryLogicVM", "class_languages_1_1_w_p_f_1_1_b_l_1_1_country_logic_v_m.html", "class_languages_1_1_w_p_f_1_1_b_l_1_1_country_logic_v_m" ],
          [ "ICountryLogicVM", "interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_country_logic_v_m.html", "interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_country_logic_v_m" ],
          [ "IEditorService", "interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", "interface_languages_1_1_w_p_f_1_1_b_l_1_1_i_editor_service" ]
        ] ],
        [ "Data", "namespace_languages_1_1_w_p_f_1_1_data.html", [
          [ "CountryVM", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m" ],
          [ "AllContinents", "class_languages_1_1_w_p_f_1_1_data_1_1_all_continents.html", "class_languages_1_1_w_p_f_1_1_data_1_1_all_continents" ]
        ] ],
        [ "UI", "namespace_languages_1_1_w_p_f_1_1_u_i.html", [
          [ "EditorWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_window" ],
          [ "EditorServiceViaWindow", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_window.html", "class_languages_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_window" ]
        ] ],
        [ "VM", "namespace_languages_1_1_w_p_f_1_1_v_m.html", [
          [ "EditorViewModel", "class_languages_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", "class_languages_1_1_w_p_f_1_1_v_m_1_1_editor_view_model" ],
          [ "MainViewModel", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model" ]
        ] ],
        [ "App", "class_languages_1_1_w_p_f_1_1_app.html", "class_languages_1_1_w_p_f_1_1_app" ],
        [ "MainWindow", "class_languages_1_1_w_p_f_1_1_main_window.html", "class_languages_1_1_w_p_f_1_1_main_window" ]
      ] ],
      [ "WpfClient", "namespace_languages_1_1_wpf_client.html", [
        [ "UI", "namespace_languages_1_1_wpf_client_1_1_u_i.html", [
          [ "EditorWindow", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window.html", "class_languages_1_1_wpf_client_1_1_u_i_1_1_editor_window" ]
        ] ],
        [ "AllContinents", "class_languages_1_1_wpf_client_1_1_all_continents.html", "class_languages_1_1_wpf_client_1_1_all_continents" ],
        [ "App", "class_languages_1_1_wpf_client_1_1_app.html", "class_languages_1_1_wpf_client_1_1_app" ],
        [ "CountryVM", "class_languages_1_1_wpf_client_1_1_country_v_m.html", "class_languages_1_1_wpf_client_1_1_country_v_m" ],
        [ "MainLogic", "class_languages_1_1_wpf_client_1_1_main_logic.html", "class_languages_1_1_wpf_client_1_1_main_logic" ],
        [ "MainVM", "class_languages_1_1_wpf_client_1_1_main_v_m.html", "class_languages_1_1_wpf_client_1_1_main_v_m" ],
        [ "MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", "class_languages_1_1_wpf_client_1_1_main_window" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];