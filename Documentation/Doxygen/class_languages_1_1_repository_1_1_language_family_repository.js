var class_languages_1_1_repository_1_1_language_family_repository =
[
    [ "LanguageFamilyRepository", "class_languages_1_1_repository_1_1_language_family_repository.html#aeb308001307c6e9b92d781dc622c3834", null ],
    [ "GetAll", "class_languages_1_1_repository_1_1_language_family_repository.html#aad52edb2fc694ea0b28bab43bfd45f7d", null ],
    [ "GetOne", "class_languages_1_1_repository_1_1_language_family_repository.html#a55efe7fcfca7018538fe5120803a871d", null ],
    [ "Insert", "class_languages_1_1_repository_1_1_language_family_repository.html#aa2e7f5907c59fdea35bfd13f8c2d89dd", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_language_family_repository.html#a865cedd1a3cc7714daeef08881c380f9", null ],
    [ "Modify", "class_languages_1_1_repository_1_1_language_family_repository.html#a20f0f06a9c603655f38887ebc7b5397b", null ],
    [ "Remove", "class_languages_1_1_repository_1_1_language_family_repository.html#a95bc15a772672c7cee0260f77595d2c1", null ]
];