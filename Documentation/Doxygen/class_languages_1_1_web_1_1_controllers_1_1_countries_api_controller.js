var class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller =
[
    [ "ApiResult", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller_1_1_api_result.html", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller_1_1_api_result" ],
    [ "CountriesApiController", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a1670f55128e8a03ddd456967def19c82", null ],
    [ "AddOneCountry", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a134fdd63b7e10e5f1d86380d1a3bce63", null ],
    [ "DelOneCountry", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a36f0650df73d787a7aa81ea41441057f", null ],
    [ "GetAll", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a5e4de0f5a8a143e21efeff4f9152e0bb", null ],
    [ "Index", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a56923f0954ee0221660ce54e2d5cabd8", null ],
    [ "ModOneCountry", "class_languages_1_1_web_1_1_controllers_1_1_countries_api_controller.html#a4fbb3f0fdf3aa8cf7489c5cec953f0c1", null ]
];