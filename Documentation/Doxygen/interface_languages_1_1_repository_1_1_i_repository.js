var interface_languages_1_1_repository_1_1_i_repository =
[
    [ "GetAll", "interface_languages_1_1_repository_1_1_i_repository.html#a2a55a593e7c3d26508452ab465196fc8", null ],
    [ "GetOne", "interface_languages_1_1_repository_1_1_i_repository.html#a0dabe967d22b6e2efa8b567c5aab17c3", null ],
    [ "Insert", "interface_languages_1_1_repository_1_1_i_repository.html#a17d8734ca4a32d12428596c5ef3e7596", null ],
    [ "Modify", "interface_languages_1_1_repository_1_1_i_repository.html#ae1b07ff12a957722b1652690b322d266", null ],
    [ "Modify", "interface_languages_1_1_repository_1_1_i_repository.html#ac8a798415788180698db01093d3d4de1", null ],
    [ "Remove", "interface_languages_1_1_repository_1_1_i_repository.html#a1151a9c0f1a2521ca3881ce9ae7f4e02", null ]
];