var class_languages_1_1_logic_1_1_logics_1_1_language_logic =
[
    [ "LanguageLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a50df33e361710e405bffb8adea86c833", null ],
    [ "LanguageLogic", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a0b1cef24f34f252d5d673dc59b8d78e5", null ],
    [ "GetAll", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#ab8328f7491e37fc8050d640c227b8524", null ],
    [ "GetOne", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a7e01dffc2ac69be37b9910538bc41b3b", null ],
    [ "Insert", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a3d1cb8e422ccd2bf834a8cf5c1594df1", null ],
    [ "LanguageFamilies", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a7e0119e3ebbd5858637afacfdafdfe34", null ],
    [ "LanguageFamiliesForTesting", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a6ba1e1e4814429953b24ef85adcd64e3", null ],
    [ "LanguagesByDifficulty", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a5c4ba3834ef9d6c53eb8f201135d7f29", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#acebcd8f293f7ebb29614ef66a420f54d", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#ab3820f66211d61c14ba61148e147cd78", null ],
    [ "NumberOfSpeakers", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a501afe6aad53d82700656ca8ba6952e2", null ],
    [ "OfficialLanguages", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#ad448af92d5b88cc7a22d0c3d6074ac54", null ],
    [ "Remove", "class_languages_1_1_logic_1_1_logics_1_1_language_logic.html#a37ae6b276e657bb96600a31bd5966326", null ]
];