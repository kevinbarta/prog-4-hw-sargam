var class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic =
[
    [ "LangfamLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#a82c49a14429e5f6f37061decaa1870f1", null ],
    [ "LangfamLangLinkLogic", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#a32fb75ea150a6ccc1a160a8fe5e108c2", null ],
    [ "GetAll", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#a6a444b11e9103a6bf5d65f06601e96b9", null ],
    [ "GetOne", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#a78ef5e0dcf4a27827acde218308821cf", null ],
    [ "Insert", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#aa5c809b6336b223a5afd466c51b0bb2d", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#a03b6d8e5984e48920351dfd78aa1f77b", null ],
    [ "Modify", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#a6c8437fe58f2d8404d1b51d8c74abee2", null ],
    [ "Remove", "class_languages_1_1_logic_1_1_logics_1_1_langfam_lang_link_logic.html#aa05946091a56c9d0c81303824254106d", null ]
];