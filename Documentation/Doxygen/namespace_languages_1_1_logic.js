var namespace_languages_1_1_logic =
[
    [ "Logics", "namespace_languages_1_1_logic_1_1_logics.html", "namespace_languages_1_1_logic_1_1_logics" ],
    [ "ILanguageLogic", "interface_languages_1_1_logic_1_1_i_language_logic.html", "interface_languages_1_1_logic_1_1_i_language_logic" ],
    [ "ILogic", "interface_languages_1_1_logic_1_1_i_logic.html", "interface_languages_1_1_logic_1_1_i_logic" ],
    [ "LogicBase", "class_languages_1_1_logic_1_1_logic_base.html", "class_languages_1_1_logic_1_1_logic_base" ],
    [ "QLanguageFamilies", "class_languages_1_1_logic_1_1_q_language_families.html", "class_languages_1_1_logic_1_1_q_language_families" ],
    [ "QLanguagesByDifficulty", "class_languages_1_1_logic_1_1_q_languages_by_difficulty.html", "class_languages_1_1_logic_1_1_q_languages_by_difficulty" ],
    [ "QNumberOfSpeakers", "class_languages_1_1_logic_1_1_q_number_of_speakers.html", "class_languages_1_1_logic_1_1_q_number_of_speakers" ],
    [ "QOfficialLanguages", "class_languages_1_1_logic_1_1_q_official_languages.html", "class_languages_1_1_logic_1_1_q_official_languages" ]
];