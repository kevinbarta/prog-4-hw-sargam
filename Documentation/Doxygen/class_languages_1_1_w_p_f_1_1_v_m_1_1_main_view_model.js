var class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#ae975b51bcd8aefba0aa19911e758b851", null ],
    [ "MainViewModel", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a4be58978623ca942cfe78850875b18b5", null ],
    [ "AddCmd", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a421b867e527c233dff3338f95e5b12a1", null ],
    [ "Countries", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#ac5779bb882208b09c271d55ce8016754", null ],
    [ "DelCmd", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#aca6e68efdaf0c9e99e1ad4bb119ae576", null ],
    [ "ModCmd", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a6fc9dc26b8c2fdf314ab5961c8eb477c", null ],
    [ "SelectedCountryVM", "class_languages_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html#a38ad63dca5810afe3c2c2da2d0192c35", null ]
];