var interface_languages_1_1_logic_1_1_i_language_logic =
[
    [ "LanguageFamilies", "interface_languages_1_1_logic_1_1_i_language_logic.html#ae52f668a6739b6210e0ac0ffa735cfed", null ],
    [ "LanguageFamiliesForTesting", "interface_languages_1_1_logic_1_1_i_language_logic.html#a2c94a3e02148c48927d2b8604e6608d6", null ],
    [ "LanguagesByDifficulty", "interface_languages_1_1_logic_1_1_i_language_logic.html#aa51a97fd0ace31e7bedc8f430baec52c", null ],
    [ "NumberOfSpeakers", "interface_languages_1_1_logic_1_1_i_language_logic.html#a5d52fbab79db8fed0ee61a2f9cbed5bb", null ],
    [ "OfficialLanguages", "interface_languages_1_1_logic_1_1_i_language_logic.html#a534644f315192ce3ec45bd16a2e512e2", null ]
];