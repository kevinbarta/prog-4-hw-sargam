var namespace_languages =
[
    [ "Data", "namespace_languages_1_1_data.html", "namespace_languages_1_1_data" ],
    [ "Logic", "namespace_languages_1_1_logic.html", "namespace_languages_1_1_logic" ],
    [ "Repository", "namespace_languages_1_1_repository.html", "namespace_languages_1_1_repository" ],
    [ "Web", "namespace_languages_1_1_web.html", "namespace_languages_1_1_web" ],
    [ "WPF", "namespace_languages_1_1_w_p_f.html", "namespace_languages_1_1_w_p_f" ],
    [ "WpfClient", "namespace_languages_1_1_wpf_client.html", "namespace_languages_1_1_wpf_client" ]
];