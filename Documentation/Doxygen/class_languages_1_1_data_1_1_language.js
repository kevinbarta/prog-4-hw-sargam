var class_languages_1_1_data_1_1_language =
[
    [ "Language", "class_languages_1_1_data_1_1_language.html#aae03d2b3b260716a362e3cffc6a37a13", null ],
    [ "Agglutinative", "class_languages_1_1_data_1_1_language.html#a7bf066be32bb54b49c84f26b7acedfe6", null ],
    [ "CountryLangLinks", "class_languages_1_1_data_1_1_language.html#aaeffefb05d671ed3f602a528741625c3", null ],
    [ "Difficulty", "class_languages_1_1_data_1_1_language.html#a35abcade9a181de645f9dc2d50479284", null ],
    [ "Id", "class_languages_1_1_data_1_1_language.html#af854251f8bfa118ea657324f94fff2de", null ],
    [ "LangfamLangLinks", "class_languages_1_1_data_1_1_language.html#a765fccfa91678a5c7aa63cecf17d02b0", null ],
    [ "Name", "class_languages_1_1_data_1_1_language.html#a055ab1889b0130b3d8db138f4255415e", null ],
    [ "NoOfNounDeclensionCases", "class_languages_1_1_data_1_1_language.html#a0326bda4a59e4ef843a1cdd0b3b2c0e7", null ],
    [ "NumberOfSpeakers", "class_languages_1_1_data_1_1_language.html#aea558b8f403f5fba43d1615957da83b9", null ],
    [ "NumberOfTenses", "class_languages_1_1_data_1_1_language.html#a671eb0bd02eaf8c4c159124df70d1f58", null ],
    [ "RankByNoSpeakers", "class_languages_1_1_data_1_1_language.html#a22c4363ce65c06917397a3469f65f383", null ]
];