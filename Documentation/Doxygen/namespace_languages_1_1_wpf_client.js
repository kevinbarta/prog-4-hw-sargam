var namespace_languages_1_1_wpf_client =
[
    [ "UI", "namespace_languages_1_1_wpf_client_1_1_u_i.html", "namespace_languages_1_1_wpf_client_1_1_u_i" ],
    [ "AllContinents", "class_languages_1_1_wpf_client_1_1_all_continents.html", "class_languages_1_1_wpf_client_1_1_all_continents" ],
    [ "App", "class_languages_1_1_wpf_client_1_1_app.html", "class_languages_1_1_wpf_client_1_1_app" ],
    [ "CountryVM", "class_languages_1_1_wpf_client_1_1_country_v_m.html", "class_languages_1_1_wpf_client_1_1_country_v_m" ],
    [ "MainLogic", "class_languages_1_1_wpf_client_1_1_main_logic.html", "class_languages_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_languages_1_1_wpf_client_1_1_main_v_m.html", "class_languages_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_languages_1_1_wpf_client_1_1_main_window.html", "class_languages_1_1_wpf_client_1_1_main_window" ]
];