var class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m =
[
    [ "CopyFrom", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a9746f3d74fd58d5cddff5ce2093e6873", null ],
    [ "Area", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a725fb30bd79597d755af94f00a12bac5", null ],
    [ "Capital", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a087391af946d74f0e9a914f79e34bb64", null ],
    [ "Continent", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a70688465e04ce5c3f3b58c7eb0310574", null ],
    [ "Id", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a4c85639da0e4503caa3817bfd847d10c", null ],
    [ "Name", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a8243360045f8ef46105b0ab42d3ae073", null ],
    [ "Population", "class_languages_1_1_w_p_f_1_1_data_1_1_country_v_m.html#a49b36a1d0edbb53bc9557c36865e0d5b", null ]
];