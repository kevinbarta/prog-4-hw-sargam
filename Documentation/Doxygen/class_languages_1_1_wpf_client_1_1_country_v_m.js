var class_languages_1_1_wpf_client_1_1_country_v_m =
[
    [ "CopyFrom", "class_languages_1_1_wpf_client_1_1_country_v_m.html#a9260e7a2068daf84c734ee773086a6f0", null ],
    [ "Area", "class_languages_1_1_wpf_client_1_1_country_v_m.html#ad37b8abcdb4ee5d37e90f6cb5bc25416", null ],
    [ "Capital", "class_languages_1_1_wpf_client_1_1_country_v_m.html#af9c40f21741ec1c5654adb0de8b03747", null ],
    [ "Continent", "class_languages_1_1_wpf_client_1_1_country_v_m.html#a801bdb20d3f87e9f5996499b0173d142", null ],
    [ "Id", "class_languages_1_1_wpf_client_1_1_country_v_m.html#a486930a7b5ebf91fc4e0178250f9d4f6", null ],
    [ "Name", "class_languages_1_1_wpf_client_1_1_country_v_m.html#a2256ab9f9d20e9c789b4898b0b60f041", null ],
    [ "Population", "class_languages_1_1_wpf_client_1_1_country_v_m.html#a9c3e6e63aa1768f11e14bd18c40485a7", null ]
];