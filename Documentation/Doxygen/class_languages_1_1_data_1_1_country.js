var class_languages_1_1_data_1_1_country =
[
    [ "Country", "class_languages_1_1_data_1_1_country.html#a06e8728c6dc9add1674f98149c02c2bd", null ],
    [ "CopyFrom", "class_languages_1_1_data_1_1_country.html#a01dd2aa32fe58858978cb37a77d19b5d", null ],
    [ "Area", "class_languages_1_1_data_1_1_country.html#a0363f1858c0ab4710ec1c44417d8d1b9", null ],
    [ "Capital", "class_languages_1_1_data_1_1_country.html#ac65428aff67418d4f039c97258c43c12", null ],
    [ "Continent", "class_languages_1_1_data_1_1_country.html#a3cb130b90a74e9557c78ee65b8c42ece", null ],
    [ "CountryLangLinks", "class_languages_1_1_data_1_1_country.html#a5bd6f1c472031f865773f8ea129ea514", null ],
    [ "Id", "class_languages_1_1_data_1_1_country.html#ac3d48e91c09ba57cc118f6f166bf8cc4", null ],
    [ "Name", "class_languages_1_1_data_1_1_country.html#aff60d40b367548e5269d9b4700b3b971", null ],
    [ "Population", "class_languages_1_1_data_1_1_country.html#ad34197b5d506be97a9ced701725c1131", null ]
];